import json
import requests

abbreviations = {
    "AL": "Alabama",
    "AK": "Alaska",
    "AS": "American Samoa",
    "AZ": "Arizona",
    "AR": "Arkansas",
    "CA": "California",
    "CO": "Colorado",
    "CT": "Connecticut",
    "DE": "Delaware",
    "DC": "District Of Columbia",
    "FM": "Federated States Of Micronesia",
    "FL": "Florida",
    "GA": "Georgia",
    "GU": "Guam",
    "HI": "Hawaii",
    "ID": "Idaho",
    "IL": "Illinois",
    "IN": "Indiana",
    "IA": "Iowa",
    "KS": "Kansas",
    "KY": "Kentucky",
    "LA": "Louisiana",
    "ME": "Maine",
    "MH": "Marshall Islands",
    "MD": "Maryland",
    "MA": "Massachusetts",
    "MI": "Michigan",
    "MN": "Minnesota",
    "MS": "Mississippi",
    "MO": "Missouri",
    "MT": "Montana",
    "NE": "Nebraska",
    "NV": "Nevada",
    "NH": "New Hampshire",
    "NJ": "New Jersey",
    "NM": "New Mexico",
    "NY": "New York",
    "NC": "North Carolina",
    "ND": "North Dakota",
    "MP": "Northern Mariana Islands",
    "OH": "Ohio",
    "OK": "Oklahoma",
    "OR": "Oregon",
    "PW": "Palau",
    "PA": "Pennsylvania",
    "PR": "Puerto Rico",
    "RI": "Rhode Island",
    "SC": "South Carolina",
    "SD": "South Dakota",
    "TN": "Tennessee",
    "TX": "Texas",
    "UT": "Utah",
    "VT": "Vermont",
    "VI": "Virgin Islands",
    "VA": "Virginia",
    "WA": "Washington",
    "WV": "West Virginia",
    "WI": "Wisconsin",
    "WY": "Wyoming"
}

# PIE CHART
def requestsRepsForParties():
	url = "https://api.congressand.me/api/Representatives?page=1&results_per_page=1000"
	r = requests.get(url)
	data = r.json()
	print(len(data['objects']))
	parties = {}

	for i in range(len(data['objects'])):
		party = data['objects'][i]['party']
		if party in parties:
			parties[party] = parties[party] + 1
		else:
			parties[party] = 1
	
	with open('pie_chart.json', 'w') as outfile:
		json.dump(parties, outfile)

# STATE MAP
def requestRepsForStates():
	url = "https://api.congressand.me/api/Representatives?page=1&results_per_page=1000"
	r = requests.get(url)
	data = r.json()
	# print(data['objects'][0])
	states = {}

	for i in range(len(data['objects'])):
		state = data['objects'][i]['state']
		state = abbreviations[state.upper()]
		if state in states:
			states[state] = states[state] + 1
		else:
			states[state] = 1
	# for state in states:
	# 	print(state, ": ", states[state])
	
	with open('state_map.json', 'w') as outfile:
		json.dump(states, outfile)

# BUBBLE CHART
def requestRepsForIssues():
	url = "https://api.congressand.me/api/Representatives?page=1&results_per_page=1000"
	r = requests.get(url)
	data = r.json()
	# print(data['objects'][0]["issues"])
	issues = {}

	for i in range(len(data['objects'])):
		issue = data['objects'][i]['issues']
		allIssues = issue.split(",")
		for x in allIssues:
			x = x.strip()
			if x in issues:
				issues[x] = issues[x] + 1
			else:
				if(len(x) > 3):
					issues[x] = 1
	
	with open('bubble_chart.json', 'w') as outfile:
		json.dump(issues, outfile)

requestsRepsForParties()
requestRepsForStates()
requestRepsForIssues()

