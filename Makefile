deploy-frontend:
	cd client; pip install awscli; npm run deploy

deploy-backend:
	cd server; pip install awsebcli; eb deploy

deploy: deploy-frontend deploy-backend

build-frontend:
	cd client; npm install; CI=false npm run build

build: build-frontend

test-frontend:
	cd client; npm test

test-backend:
	cd server; pip install -r requirements.txt; python ./tests/unit_tests.py 

test-api:
	npm install -g newman
	newman run ./server/tests/api_tests.postman_collection.json

e2e:
	cd client; npm run e2e

test: test-api test-backend test-frontend

kill-docker:
	for c in $$(docker ps -q); do docker kill $$c; done

docker: kill-docker
	docker build -t server ./server
	docker run -d -p 5000:5000 server

