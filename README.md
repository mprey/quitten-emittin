# Quitten' Emittin'

Our objective is to educate people on environmental impact and emissions, in an effort to promote public action against irresponsible environmental practices.

API Documentation: https://documenter.getpostman.com/view/570117/SVtR1A1J?version=latest

Git SHA for grading: [1917725b2f666add4bdfe9be33d25a95f08f0417](https://gitlab.com/mprey/quitten-emittin/commit/1917725b2f666add4bdfe9be33d25a95f08f0417)  
Project Leader: Mason Prey

Frontend: https://quittinemittin.me  
Backend: https://api.quittinemittin.me

## Member 1

Name: Mason Prey  
EID: mpp632  
GitLab ID: mprey  
Estimated Completion Time: 12 hours  
Actual completion time: 20 hours  
Comments: N/A  

## Member 2

Name: Mark Ellis  
EID: mme839  
GitLab ID: mmellis  
Estimated Completion Time: 20 hours 
Actual completion time: 15 hours  
Comments: N/A  

## Member 3

Name: Varun Rajaram  
EID: vjr435  
GitLab ID: varunrajaram  
Estimated Completion Time: 15 hours  
Actual completion time: 20 hours  
Comments: N/A  

## Member 4

Name: Jack Rentz  
EID: jrr4555  
GitLab ID: jackrentz  
Estimated Completion Time: 15  
Actual completion time: 20 hours  
Comments: N/A  

## Member 5

Name: Nidhin Varghese  
EID: nbv222  
GitLab ID: nidhinbv  
Estimated Completion Time: 30  
Actual completion time: 20 hours  
Comments: N/A  

## Member 6

Name: Allen Zhang  
EID: az6879  
GitLab ID: aykzhang  
Estimated Completion Time: 15  
Actual completion time: 20 hours  
Comments: N/A  

Link to website: https://quittinemittin.me  
Link to GitLab Pipelines: https://gitlab.com/mprey/quitten-emittin/pipelines  
Path to front-end code: client/  
Path to back-end server: server/  
