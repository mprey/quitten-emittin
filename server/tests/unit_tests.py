import os
import unittest
import sys
import json
import requests
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from server import app, api_prefix, Config

STATES= ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "New Hampshire"]

EMISSION_TYPES = ['Coal', 'Commercial', 'Electricity', 'Industrial', 'Natural Gas', 'Petroleum', 'Residential', 'Transportation']

LAW_IDS = [1,2,3,4,5]

LAWS_PER_PAGE = 9
STATES_PER_PAGE = 9

GITLAB_ENDPOINT = 'https://gitlab.com/api/v4/projects/'

class Unit_Tests(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True
    def tearDown(self):
        pass
    
    
    # EMISSIONS TESTS
    def test_emissions_model_status_code(self):
        result = self.app.get(api_prefix + '/emissions')
        self.assertEqual(result.status_code, 200)
    
    def test_emissions_model_data(self):
        result = self.app.get(api_prefix + '/emissions')
        emission_types = json.loads(result.data)['data']
        assert len(emission_types) == 8
        for et in emission_types:
            assert et['emission_name'] in EMISSION_TYPES

    def test_emissions_filtering_status_code(self):
        result = self.app.get(api_prefix + '/emissions?filters={"worst_state":"Texas","best_state":"Vermont"}')
        self.assertEqual(result.status_code, 200)
    
    def test_emissions_filtering_data(self):
        result = self.app.get(api_prefix + '/emissions?filters={"worst_state":"Texas","best_state":"Vermont"}')
        emission_types = json.loads(result.data)['data']
        assert len(emission_types) == 3
        for et in emission_types:
            assert et['emission_name'] in EMISSION_TYPES
            assert et['best_state'] == "Vermont"
            assert et['worst_state'] == "Texas"
    
    def test_emissions_sorting_status_code(self):
        result = self.app.get(api_prefix + '/emissions?sortName=worst_year&sortDir=desc')
        self.assertEqual(result.status_code, 200)
    
    def test_emissions_sorting_data(self):
        result = self.app.get(api_prefix + '/emissions?sortName=worst_year&sortDir=desc')
        emission_types = json.loads(result.data)['data']
        assert len(emission_types) == 8
        prev_year = 2020
        for et in emission_types:
            current_year = et['worst_year']
            assert current_year <= prev_year
            prev_year = current_year
    
    def test_emissions_search_status_code(self):
        result = self.app.get(api_prefix + '/search/emissions?search=Coal')
        self.assertEqual(result.status_code, 200)
    
    def test_emissions_search_data(self):
        result = self.app.get(api_prefix + '/search/emissions?search=Coal')
        emission_type = json.loads(result.data)['data']
        assert len(emission_type) == 1
        assert emission_type[0]['emission_name'] == "Coal"
    
    def test_emissions_instance_status_codes(self):
        for et in EMISSION_TYPES:
            url = api_prefix + '/emissions/' + et
            result = self.app.get(url)
            self.assertEqual(result.status_code, 200)

    def test_emissions_instance_data(self):
        for et in EMISSION_TYPES:
            url = api_prefix + '/emissions/' + et
            result = self.app.get(url)
            emisison_data = json.loads(result.data)['data']
            assert 'description' in emisison_data and 'emission_name' in emisison_data and \
                'emission_pic_url' in emisison_data and 'global' in emisison_data and 'states' in emisison_data


    # STATES TESTS
    def test_states_model_status_code(self):
        result = self.app.get(api_prefix + '/states')
        self.assertEqual(result.status_code, 200)
    
    def test_states_model_data(self):
        # Test data on first page
        result = self.app.get(api_prefix + '/states')
        states = json.loads(result.data)['data']
        for s in states:
            assert s['state_name'] in STATES
        assert len(states) == STATES_PER_PAGE

        # Test pagination for states model
        empty_page = False
        total_states = 0 #STATES_PER_PAGE
        page_num = 1
        while not empty_page:
            url = api_prefix + '/states?page=' + str(page_num)
            result = self.app.get(url)
            states_in_page = json.loads(result.data)['data']
            total_states += len(states_in_page)
            page_num += 1

            if len(states_in_page) == 0:
                empty_page = True
        assert total_states == 50
    
    def test_states_filtering_status_code(self):
        result = self.app.get(api_prefix + '/states?filters={"laws_passed":3,"largest_contributor":"Petroleum"}')
        self.assertEqual(result.status_code, 200)
    
    def test_states_filtering_data(self):
        result = self.app.get(api_prefix + '/states?filters={"laws_passed":3,"largest_contributor":"Petroleum"}')
        states = json.loads(result.data)['data']
        for s in states:
            assert s['laws_passed'] >= 3
            assert s['largest_contributor'] == "Petroleum"
    
    def test_states_sorting_status_code(self):
        result = self.app.get(api_prefix + '/states?sortName=largest_contributor&sortDir=asc')
        self.assertEqual(result.status_code, 200)
    
    def test_states_sorting_data(self):
        result = self.app.get(api_prefix + '/states?sortName=largest_contributor&sortDir=asc&page=2')
        states = json.loads(result.data)['data']
        for s in states:
            assert s['largest_contributor'] == "Electricity"
    
    def test_states_search_status_code(self):
        result = self.app.get(api_prefix + '/search/states?search=Texas')
        self.assertEqual(result.status_code, 200)
    
    def test_states_search_data(self):
        result = self.app.get(api_prefix + '/search/states?search=Texas')
        state = json.loads(result.data)['data']
        assert len(state) == 1
        assert state[0]['state_name'] == "Texas"
        assert 'id' in state[0].keys()
       
    def test_states_instance_status_codes(self):
        for s in STATES:
            url = api_prefix + '/states/' + s
            result = self.app.get(url)
            self.assertEqual(result.status_code, 200)

    def test_states_instance_data(self):
        for s in STATES:
            url = api_prefix + '/states/' + s
            result = self.app.get(url)
            state_data = json.loads(result.data)['data']
            assert 'emissions' in state_data and 'flag_url' in state_data and 'num_representatives' in state_data and \
                'population' in state_data and 'state_name' in state_data
            
    
    # LAWS TESTS
    def test_laws_model_status_code(self):
        result = self.app.get(api_prefix + '/laws')
        self.assertEqual(result.status_code, 200)
    
    def test_laws_model_data(self):
        # Test data on first page
        result = self.app.get(api_prefix + '/laws')
        laws = json.loads(result.data)['data']
        for l in laws:
            assert 'creation_date' in l and 'description' in l and 'emissions' in l and \
                   'law_name' in l and 'representatives' in l and 'state_name' in l and \
                   'url' in l and 'updated_date' in l
        assert len(laws) == LAWS_PER_PAGE

        # Test pagination for states model
        page_num = 1
        while page_num < 10:
            url = api_prefix + '/laws?page=' + str(page_num)
            result = self.app.get(url)
            laws_in_page = json.loads(result.data)['data']
            page_num += 1
            for l in laws_in_page:
                assert 'creation_date' in l and 'description' in l and 'emissions' in l and \
                    'law_name' in l and 'representatives' in l and 'state_name' in l and \
                    'url' in l and 'updated_date' in l
            assert len(laws_in_page) == LAWS_PER_PAGE
    
    def test_laws_filtering_status_code(self):
        result = self.app.get(api_prefix + '/laws?filters={"emissions": "Coal"}')
        self.assertEqual(result.status_code, 200)
    
    def test_laws_filtering_data(self):
        result = self.app.get(api_prefix + '/laws?filters={"emissions": "Coal"}')
        laws = json.loads(result.data)['data']
        for l in laws:
            assert "Coal" in l['emissions']
    
    def test_laws_sorting_status_code(self):
        result = self.app.get(api_prefix + '/laws?sortName=creation_date&sortDir=desc')
        self.assertEqual(result.status_code, 200)
    
    def test_laws_sorting_data(self):
        result = self.app.get(api_prefix + '/laws?sortName=creation_date&sortDir=desc')
        laws = json.loads(result.data)['data']

        for l in laws:
            assert l['year'] == 2019
    
    def test_laws_search_status_code(self):
        result = self.app.get(api_prefix + '/search/laws?search=Texas')
        self.assertEqual(result.status_code, 200)
    
    def test_laws_search_data(self):
        result = self.app.get(api_prefix + '/search/laws?search=Texas')
        states = json.loads(result.data)['data']
        for s in states:
            assert "Texas" in s['description'] or s in ['law_name']
        
    def test_laws_instance_status_codes(self):
        for l in LAW_IDS:
            url = api_prefix + '/laws/' + str(l)
            result = self.app.get(url)
            self.assertEqual(result.status_code, 200)

    def test_laws_instance_data(self):
        for l in LAW_IDS:
            url = api_prefix + '/laws/' + str(l)
            result = self.app.get(url)
            law_data = json.loads(result.data)['data']
            assert 'creation_date' in law_data and 'description' in law_data and 'emissions' in law_data and \
                   'law_name' in law_data and 'representatives' in law_data and 'state_name' in law_data and \
                   'url' in law_data and 'updated_date' in law_data 

    
    # GLOBAL SEARCH TESTS
    def test_global_search_status_code(self):
        result = self.app.get(api_prefix + '/search/laws?search=Coal')
        self.assertEqual(result.status_code, 200)

    def test_global_search_data(self):
        result = self.app.get(api_prefix + '/search/global?search=Vermont')
        data = json.loads(result.data)['data']
        emissions_data = data['emissions']
        assert len(emissions_data) == 0
        laws_data = data['laws']
        for l in laws_data:
            assert "Vermont" in l['description'] or "Vermont" in l['law_name']
        states_data = data['states']
        assert states_data[0]['state_name'] == "Vermont"


    # GITLAB TESTS
    def test_gitlab_codes(self):
        # Check commits
        url = GITLAB_ENDPOINT + Config.GITLAB_PROJECT_ID + '/repository/commits?per_page=500'
        self.assertEqual(requests.get(url).status_code,200)

        # Check issues
        url = GITLAB_ENDPOINT + Config.GITLAB_PROJECT_ID + '/issues?per_page=500'
        self.assertEqual(requests.get(url).status_code, 200)

if __name__ == "__main__":
    unittest.main()