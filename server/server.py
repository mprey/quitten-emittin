from flask import Flask, request, jsonify
from flask_cors import CORS
from flask_restful import Resource, Api, reqparse
from flask_caching import Cache
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, Integer, String, Float, func
from sqlalchemy.dialects.postgresql import ARRAY, TEXT
from sqlalchemy.orm import sessionmaker
from config import Config
from math import ceil
from datetime import datetime
import json
import requests
import csv

app = Flask(__name__)
api = Api(app)
cors = CORS(app)
cache = Cache(app, config={'CACHE_TYPE': 'simple'})

engine = create_engine(Config.POSTGRES_CONN)
Session = sessionmaker(bind=engine)
BaseModel = declarative_base()
connection = engine.connect()

# DB MODELS

class State(BaseModel):
    __tablename__ = 'states'
    id = Column(Integer, primary_key=True)
    state_name = Column(String)
    population = Column(Integer)
    flag_url = Column(String)
    num_representatives = Column(Integer)

class Emission(BaseModel):
    __tablename__ = 'emissions'
    id = Column(Integer, primary_key=True)
    emission_name = Column(String)
    state_name = Column(String)
    year = Column(Integer)
    total_emissions = Column(Float)
    emission_pic_url = Column(String)
    emission_description = Column(String)

class Law(BaseModel):
    __tablename__ = 'laws'
    id = Column(Integer, primary_key=True)
    state_name = Column(String)
    creation_date = Column(String)
    updated_date = Column(String)
    emissions = Column(ARRAY(TEXT))
    law_name = Column(String)
    description = Column(String)
    representatives = Column(String)
    url = Column(String)

class Tip(BaseModel):
    __tablename__ = 'tips'
    id = Column(Integer, primary_key=True)
    emission_name = Column(String)
    description = Column(String)

BaseModel.metadata.create_all(engine)

# ENDPOINTS

class Paginated(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        for (key, value) in self.args.items():
            self.parser.add_argument(key, default=value['default'], type=value['type'])

    def get_idx_range(self, args, zero_based=False):
        startIdx = (args['page'] - 1) * args['resultsPerPage'] + 1
        stopIdx = startIdx + args['resultsPerPage'] - 1
        if zero_based:
            return (startIdx - 1, stopIdx)
        return (startIdx, stopIdx)

    def should_filter(self, item, filters):
        matches = set(item).intersection(set(filters))
        if not matches:
            return True

        for match in matches:
            value = item[match]
            filter_by = filters[match]
            filter_type = self.filters[match]
            if filter_type == "number" and value < filter_by:
                return True
            elif filter_type == "list" and filter_by not in value:
                return True
            elif filter_type == "string" and value != filter_by:
                return True
        return False

    def parse_filters(self, items, args):
        available_filters = dict((key, set()) for key in self.filters)
        for item in items:
            for key, ftype in self.filters.items():
                if ftype == "string":
                    available_filters[key].add(item[key])
                elif ftype == "list":
                    available_filters[key].update(item[key])
        available_filters = dict((key, list(value)) for key, value in available_filters.items())

        filters = args['filters'] = json.loads(args['filters'])
        if not filters:
            return items, available_filters
        return [item for item in items if not self.should_filter(item, filters)], available_filters


    @property
    def filters(self):
        return {}

    @property
    def args(self):
        return {
            "resultsPerPage": {
                "default": 9,
                "type": int
            },
            "page": {
                "default": 1,
                "type": int
            },
            "sortName": {
                "default": "",
                "type": str
            },
            "sortDir": {
                "default": "",
                "type": str
            },
            "filters": {
                "default": "{}",
                "type": str
            }
        }

class Search(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        for (key, value) in self.args.items():
            self.parser.add_argument(key, default=value['default'], type=value['type'])

    @property
    def args(self):
        return {
            "search": {
                "default": "",
                "type": str
            }
        }

class GitLab(Resource):
    def get(self):
        # using the GitLab API, fetch statistics for the About page
        results = {
            "commits": self.get_commits(),
            "issues": self.get_issues(),
            "tests": self.get_tests()
        }
        return results
    
    def get_commits(self):
        results = []
        page = 1
        while True:
            endpoint = 'https://gitlab.com/api/v4/projects/' + Config.GITLAB_PROJECT_ID + '/repository/commits?per_page=100&page=' + str(page)
            result = requests.get(endpoint).json()
            if len(result) == 0:
                break
            results += result
            page += 1
        return results

    def get_issues(self):
        endpoint = 'https://gitlab.com/api/v4/projects/' + Config.GITLAB_PROJECT_ID + '/issues?per_page=500'
        return requests.get(endpoint).json()

    def get_tests(self):
        with open('tests.json') as json_file:
            return json.load(json_file)

class StatesModel(Paginated):

    @cache.cached(timeout=Config.CACHE_TIMEOUT, query_string=True)
    def get(self):
        args = self.parser.parse_args()
        s = Session()
        
        (startIdx, stopIdx) = self.get_idx_range(args)
        states = s.query(State).all()

        q = []
        for i in states:
            stateEmm = s.query(Emission).filter_by(state_name = i.state_name.replace("_", " "))
            a = {'state_name': i.state_name, 
                'population': i.population,
                'flag_url': i.flag_url,
                'num_representatives': i.num_representatives,
                'emissions': 0}

            q_contributors = {}
            for x in stateEmm:
                a['emissions'] += x.total_emissions
                q_contributors[x.emission_name] = x.total_emissions
            a['largest_contributor'] = max(q_contributors, key = q_contributors.get)
            a['laws_passed'] = s.query(Law).filter_by(state_name = i.state_name.replace("_", " ")).count()
            q.append(a)
        
        

        sortCol = args['sortName']
        sortDir = args['sortDir']
        
        reverse = False
        if(sortDir == "desc"):
            reverse = True

        if sortCol != "":
            q = sorted(q, key = lambda i: i[sortCol], reverse = reverse)

        q, filters = self.parse_filters(q, args)

        args['lastPage'] = ceil(len(q) / args['resultsPerPage'])
        q = q[startIdx - 1:stopIdx]

        s.close()
        return jsonify({"args": args, "data": q, "filters": filters})
        
    @property
    def filters(self):
        return {
            "population": "number",
            "num_representatives": "number",
            "emissions": "number",
            "largest_contributor": "string",
            "laws_passed": "number"
        }

class StatesInstance(Resource):
    @cache.cached(timeout=Config.CACHE_TIMEOUT, query_string=True)
    def get(self, state_name):
        s = Session()
        state = s.query(State).filter_by(state_name = state_name).first()
        emissions = [i[0] for i in s.query(Emission.emission_name).group_by(Emission.emission_name).all()]

        q_emissions = {}
        totals = {}
        grandTotal = 0.0
        for emission in emissions:
            emission_result = {}
            total = 0.0
            for i in s.query(Emission).filter_by(state_name = state_name, emission_name = emission).all():
                emission_result[i.year] = i.total_emissions
                total += float(i.total_emissions)
            grandTotal += float(total)
            q_emissions[emission] = emission_result
            totals[emission] = format(total, '.2f')

        # Calculate the percantage each emission contributes on a state level
        percentages = {}
        for emission in emissions:
            total = float(totals[emission])
            percent = total / grandTotal
            percent *= 100
            percentages[emission] = format(percent, '.2f')

        # grandTotal = 0.0
        # totals2 = {}

        # # initilize all totals to 0
        # for emission in emissions:
        #     totals2[emission] = 0.0

        # # Calculte the total of each emission on a national level
        # for st in states:
        #     for emission in emissions:
        #         total = 0.0
        #         for i in s.query(Emission).filter_by(state_name = st.state_name, emission_name = emission).all():
        #             total += float(i.total_emissions)
        #         totals2[emission] = float(totals2[emission]) + total

        # # Calculate the percentage of each emission on a national level
        # national_percentages = {}
        # for emission in emissions:
        #     total = float(totals[emission])
        #     percent = total / float(totals2[emission])
        #     percent *= 100
        #     national_percentages[emission] = format(percent, '.2f')

        q = {
            'state_name': state.state_name,
            'population': state.population,
            'flag_url': state.flag_url,
            'num_representatives': state.num_representatives,
            'emissions': q_emissions,
            'total' : totals,
            'emission_percentages': percentages,
        }

        legislation = s.query(Law).filter_by(state_name = state_name).all()
        q['legislation'] = [{"id": i.id, "name": i.law_name} for i in legislation]
        
        s.close()
        return jsonify({"args": {}, "data": q})

class EmissionsModel(Paginated):

    @cache.cached(timeout=Config.CACHE_TIMEOUT, query_string=True)
    def get(self):
        args = self.parser.parse_args()
        s = Session()

        emissions = [i[0] for i in s.query(Emission.emission_name).group_by(Emission.emission_name).all()]
        (startIdx, stopIdx) = self.get_idx_range(args, zero_based=True)
        # emissions = emissions[startIdx:stopIdx]

        q_list = []
        for emission in emissions:
            worst_year = -1
            worst_state = ""
            best_state = ""
            
            year_query = s.query(Emission).filter_by(emission_name=emission).filter_by(state_name='United States')
            year_max = -1
            for q in year_query:
                if q.total_emissions > year_max:
                    year_max = q.total_emissions
                    worst_year = q.year
            
            state_query = s.query(Emission).filter_by(emission_name=emission).filter_by(year='2016').all()
            state_max = -1
            state_min = 99999999999
            for q in state_query:
                if q.total_emissions > state_max and q.state_name not in ['United States','District of Columbia']:
                    state_max = q.total_emissions
                    worst_state = q.state_name         
                if q.total_emissions < state_min and q.state_name not in ['United States','District of Columbia']:
                    state_min = q.total_emissions
                    best_state = q.state_name        
            
            query = s.query(Emission).filter_by(emission_name=emission).filter_by(year='2016').filter_by(state_name='United States').first()
            json_format = {"emission_name": query.emission_name,
                           "total_emissions": query.total_emissions,
                           "emission_pic_url": query.emission_pic_url,
                           "best_state": best_state,
                           "worst_year": worst_year,
                           "worst_state": worst_state}

            json_format['laws_passed'] = s.query(Law).filter(Law.emissions.contains([emission])).count()


            q_list.append(json_format)




        sortCol = args['sortName']
        sortDir = args['sortDir']
        
        reverse = False
        if(sortDir == "desc"):
            reverse = True

        if sortCol != "":
            q_list = sorted(q_list, key = lambda i: i[sortCol], reverse = reverse)

        q_list, filters = self.parse_filters(q_list, args)

        args['lastPage'] = ceil(len(q_list) / args['resultsPerPage'])
        q_list = q_list[startIdx: stopIdx]

        s.close()
        return jsonify({"args": args, "data": q_list, "filters": filters})

    @property
    def filters(self):
        return {
            "total_emissions": "number",
            "best_state": "string",
            "worst_state": "string",
            "worst_year": "string",
            "laws_passed": "number"
        }

class EmissionsInstance(Resource):
    @cache.cached(timeout=Config.CACHE_TIMEOUT, query_string=True)
    def get(self, emission_name):
        # Get the emission type parameter from the URL
        emission_name = emission_name.replace('_',' ').title()
        s = Session()
        return_dict = {}
        worst_year = ""
        worst_state = ""
        best_state = ""

        query = s.query(Emission).filter_by(emission_name=emission_name).first()
        return_dict['emission_name'] = query.emission_name
        return_dict['description'] = query.emission_description
        return_dict['emission_pic_url'] = query.emission_pic_url

        
        states = {}
        globals = {}
        query = s.query(Emission).filter_by(emission_name=emission_name).all()
        for q in query:
            if q.state_name not in ['United States','District of Columbia']:
                if q.state_name in states:
                    states[q.state_name][q.year] = q.total_emissions
                else:
                    states[q.state_name] = {q.year: q.total_emissions}

            else:
                globals[q.year] = q.total_emissions

        best_state = min(states.keys(), key=(lambda k: sum(states[k].values())))
        worst_state = max(states.keys(), key=(lambda k: sum(states[k].values())))
        worst_year = max(globals.keys(), key=(lambda k: globals[k]))

        return_dict['states'] = states
        return_dict['global'] = globals
        return_dict['worst_state'] = worst_state
        return_dict['best_state'] = best_state
        return_dict['worst_year'] = worst_year

        query = s.query(Tip).filter_by(emission_name=emission_name).all()
        return_dict['tips'] = [i.description for i in query]

        legislation = s.query(Law).filter(Law.emissions.contains([emission_name])).all()
        return_dict['legislation'] = [{"id": i.id, "name": i.law_name} for i in legislation]
        return_dict['laws_passed'] = len(return_dict['legislation'])
        s.close()
        return jsonify({"args": {}, "data": return_dict})

class EmissionsSearch(Search):
    @cache.cached(timeout=Config.CACHE_TIMEOUT, query_string=True)
    def get(self):
        args = self.parser.parse_args()
        temp = args['search']
        search = temp.split(" ")
        q = self.search(search)
        return jsonify({"args": args, "data": q})
    def search(self, terms):
        s = Session()
        q = []
        emissions = s.query(Emission).filter(Emission.emission_name.like('abcde'))
        for term in terms:
            nameQuery = s.query(Emission).filter(func.lower(Emission.emission_name)
            	.like('%'+term.lower()+'%')).filter_by(year='2016').filter_by(state_name='United States')

            descQuery = s.query(Emission).filter(func.lower(Emission.emission_description)
            	.like('%'+term.lower()+'%')).filter_by(year='2016').filter_by(state_name='United States')

            emissions = emissions.union(nameQuery.union(descQuery))
        emissions = emissions.all()
        for i in emissions:
            a = {'emission_name': i.emission_name,
                 'description': i.emission_description}
            q.append(a)
        s.close()
        return q

class LawsModel(Paginated):

    @cache.cached(timeout=Config.CACHE_TIMEOUT, query_string=True)
    def get(self):
        args = self.parser.parse_args()

        s = Session()

        (startIdx, stopIdx) = self.get_idx_range(args)
        #laws = s.query(Law).filter(Law.id.between(startIdx, stopIdx)).all()
        laws = s.query(Law).all()

        q = []
        for i in laws:
            creation_date = datetime.strptime(i.creation_date, '%m/%d/%y')
            updated_date = datetime.strptime(i.updated_date, '%m/%d/%y')

            a = {'id': i.id,
                'state_name': i.state_name, 
                'creation_date': creation_date.strftime('%Y/%m/%d'),
                'updated_date': updated_date.strftime('%Y/%m/%d'),
                'year': int(creation_date.strftime('%Y')),
                'law_name': i.law_name,
                'description': i.description,
                'representatives': sorted(i.representatives.split(", ")),
                'url': i.url,
                'emissions': i.emissions}

            q.append(a)

        s.close()

        sortCol = args['sortName']
        sortDir = args['sortDir']
        
        reverse = False
        if(sortDir == "desc"):
            reverse = True

        if sortCol != "":
            if(sortCol == "emissions" or sortCol == "representatives"):
                q = sorted(q, key = lambda i: len(i[sortCol]), reverse = reverse)
            else:
                q = sorted(q, key = lambda i: i[sortCol], reverse = reverse)

        q, filters = self.parse_filters(q, args)
        args['lastPage'] = ceil(len(q) / args['resultsPerPage'])
        q = q[startIdx-1:stopIdx]
        
        
        return jsonify({"args": args, "data": q, "filters": filters})

    @property
    def filters(self):
        return {
            "state_name": "string",
            "creation_date": "string",
            "law_name": "string",
            "representatives": "list",
            "emissions": "list"
        }

class LawsInstance(Resource):
    @cache.cached(timeout=Config.CACHE_TIMEOUT, query_string=True)
    def get(self, id):
        s = Session()
        lawObj = s.query(Law).filter_by(id = id).first()
        q = {'id': lawObj.id,
            'state_name': lawObj.state_name, 
            'creation_date': lawObj.creation_date, 
            'updated_date': lawObj.updated_date, 
            'law_name': lawObj.law_name,
            'description': lawObj.description,
            'representatives': lawObj.representatives,
            'url': lawObj.url,
            'emissions': lawObj.emissions}
        
        s.close()
        return jsonify({"args": id, "data": q})

class LawsSearch(Search):
    @cache.cached(timeout=Config.CACHE_TIMEOUT, query_string=True)
    def get(self):
        args = self.parser.parse_args()
        temp = args['search']
        search = temp.split(" ")
        q = self.search(search)
        return jsonify({"args": args, "data": q})

    def search(self, terms):
        s = Session()
        q = []
        laws = s.query(Law).filter(Law.law_name.like('abcde'))
        for term in terms:
            nameQuery = s.query(Law).filter(func.lower(Law.law_name).like('%'+term.lower()+'%'))
            descQuery = s.query(Law).filter(func.lower(Law.description).like('%'+term.lower()+'%'))
            laws = laws.union(nameQuery.union(descQuery))
        laws = laws.all()
        for i in laws:
            a = {'id': i.id,
                 'law_name': i.law_name,
                 'description': i.description}
            q.append(a)
        s.close()
        return q

class StatesSearch(Search):
    @cache.cached(timeout=Config.CACHE_TIMEOUT, query_string=True)
    def get(self):
        args = self.parser.parse_args()
        temp = args['search']
        search = temp.split(" ")
        q = self.search(search)
        return jsonify({"args": args, "data": q})
    def search(self, terms):
        s = Session()
        q = []
        states = s.query(State).filter(State.state_name.like('abcde'))
        for term in terms:
            nameQuery = s.query(State).filter(func.lower(State.state_name).like('%'+term.lower()+'%'))
            # descQuery = s.query(State).filter(func.lower(State.description).like('%'+term.lower()+'%'))
            states = states.union(nameQuery)
        states = states.all()
        for i in states:
            a = {'id': i.id,
                 'state_name': i.state_name,}
            q.append(a)
        s.close()
        return q


class GlobalSearch(Search):
    @cache.cached(timeout=Config.CACHE_TIMEOUT, query_string=True)
    def get(self):
        args = self.parser.parse_args()
        temp = args['search']
        search = temp.split(" ")
        s = StatesSearch().search(search)
        l = LawsSearch().search(search)
        e = EmissionsSearch().search(search)
        q = {"states": s, "laws": l, "emissions": e}
        return jsonify({"args": args, "data": q})

class Tips(Resource):
    @cache.cached(timeout=Config.CACHE_TIMEOUT, query_string=True)
    def get(self, emission_name):
        s = Session()
        tips = s.query(Tip).filter_by(emission_name = emission_name.title()).all()
        q = []
        for i in tips:
            a = {'emission_name': i.emission_name, 
                'description': i.description}
            q.append(a)
        s.close()
        return jsonify({"args": {"emission_name": emission_name}, "data": q})

class Landing(Resource):
    def get(self):
        return 'Quittin\' Emittin\' API v1'

api_prefix = '/api/' + Config.VERSION
api.add_resource(GitLab, api_prefix + '/gitlab')
api.add_resource(StatesModel, api_prefix  + '/states')
api.add_resource(StatesInstance, api_prefix  + '/states/<string:state_name>')
api.add_resource(EmissionsModel, api_prefix + '/emissions')
api.add_resource(EmissionsInstance, api_prefix + '/emissions/<string:emission_name>')
api.add_resource(Tips, api_prefix + '/emissions/<string:emission_name>/tips')
api.add_resource(LawsModel, api_prefix + '/laws')
api.add_resource(LawsInstance, api_prefix + '/laws/<string:id>')
api.add_resource(LawsSearch, api_prefix + '/search/laws')
api.add_resource(StatesSearch, api_prefix + '/search/states')
api.add_resource(EmissionsSearch, api_prefix + '/search/emissions')
api.add_resource(GlobalSearch, api_prefix + '/search/global')
api.add_resource(Landing, '/')

# bottom of the file: run the Flask app
if __name__ == '__main__':
    print('Application running')
    app.run(host="0.0.0.0", port=5000, debug=True)
      
    # The code to import data should look something like the below code
    # Comment out the app.run() line and run server.py to import data into a model
    
    # s = Session()

    # try:
    #     with open('Laws_data.csv','r') as csv_file:
    #         reader = csv.reader(csv_file, delimiter=',')
    #         for row in reader:
    #             state_name=row[0]
    #             creation_date = row[1]
    #             updated_date = row[2]
    #             emissions = row[7].split(", ")
    #             law_name = row[3]
    #             description = row[4]
    #             representatives = row[5]
    #             url = row[6]
                
    #             law = Law(state_name=state_name, creation_date=creation_date, updated_date=updated_date, emissions=emissions, law_name=law_name, description=description, representatives=representatives, url=url)
    #             s.add(law)
    #         s.commit()
    # except:
    #     s.rollback()
    # finally:
    #     s.close()

    # id = Column(Integer, primary_key=True)
    # state_name = Column(String)
    # population = Column(Integer)
    # flag_url = Column(String)
    # num_representatives = Column(Integer)
    # # Double check that you can successfully query the data
    # q = s.query(Law).filter_by(state_name='Alabama').first()

