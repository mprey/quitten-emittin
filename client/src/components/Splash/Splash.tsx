import React from 'react';
import {
  Container,
  Row,
  Col,
  Button,
  Card,
  InputGroup,
  FormControl
} from 'react-bootstrap';
import { RouteComponentProps, withRouter } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import logo from '../../static/logo.svg';
import co2 from '../../static/co2.svg';
import law from '../../static/law.svg';
import states from '../../static/united-states-of-america.svg';

import './Splash.scss';

class Splash extends React.Component<RouteComponentProps> {
  state = {
    search: ''
  };

  render(): React.ReactNode {
    return (
      <div>
        <div className="Splash">
          <Container className="h-100">
            <Row className="h-100">
              <Col lg={7} className="my-auto">
                <div className="header-content mx-auto">
                  <h2 className="mb-5">
                    Our objective is to educate people on environmental impact
                    and emissions, in an effort to promote public action against
                    irresponsible environmental practices.
                  </h2>
                  <Button
                    id="btn-outline"
                    href="#second"
                    size="lg"
                    className="btn-outline"
                  >
                    Start Exploring
                  </Button>
                </div>
              </Col>
              <Col lg={5} className="my-auto">
                <img
                  src={logo}
                  className="img-fluid"
                  alt="logo"
                  style={{ height: '350px' }}
                />
              </Col>
            </Row>
          </Container>
        </div>
        <section className="DataCards bg-primary text-center" id="second">
          <Container>
            <Row className="justify-content-center">
              <h2>Check out our data on nation-wide climate change topics.</h2>
            </Row>
            <Row>
              <Col md={4}>
                <Card className="h-100">
                  <Card.Body>
                    <Card.Title>
                      <img src={co2} alt="co2" />
                      <br />
                      Emissions
                    </Card.Title>
                    <Card.Text>
                      The most prominent sources of CO2 emissions and the metric
                      tons.
                    </Card.Text>
                    <LinkContainer to="/emissions">
                      <Button
                        id="explore-emissions"
                        href="/emissions"
                        size="sm"
                      >
                        Explore The Data
                      </Button>
                    </LinkContainer>
                  </Card.Body>
                </Card>
              </Col>
              <Col md={4}>
                <Card className="h-100">
                  <Card.Body>
                    <Card.Title>
                      <img src={states} alt="co2" />
                      <br />
                      States
                    </Card.Title>
                    <Card.Text>
                      All 50 states and data related to their CO2 emissions by
                      year.
                    </Card.Text>
                    <LinkContainer to="/states">
                      <Button id="explore-states" href="/states" size="sm">
                        Explore The Data
                      </Button>
                    </LinkContainer>
                  </Card.Body>
                </Card>
              </Col>
              <Col md={4}>
                <Card className="h-100">
                  <Card.Body>
                    <Card.Title>
                      <img src={law} alt="co2" />
                      <br />
                      Laws
                    </Card.Title>
                    <Card.Text>
                      The different laws passed trying to combat climate change
                      recently.
                    </Card.Text>
                    <LinkContainer to="/laws">
                      <Button id="explore-laws" href="/laws" size="sm">
                        Explore The Data
                      </Button>
                    </LinkContainer>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
            <Row className="justify-content-center search">
              <h3 className="w-100">Search our website</h3>
              <InputGroup className="input mb-3">
                <FormControl
                  type="text"
                  placeholder="Search something..."
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    this.setState({ search: e.target.value })
                  }
                />
                <InputGroup.Append>
                  <Button
                    onClick={() =>
                      this.props.history.push(
                        `/search/global/${this.state.search}`
                      )
                    }
                  >
                    Search
                  </Button>
                </InputGroup.Append>
              </InputGroup>
            </Row>
          </Container>
        </section>
      </div>
    );
  }
}

export default withRouter(Splash);
