import React from 'react';
import logo from '../../static/logo.svg';

import './Background.scss';

interface Props {
  useImage: boolean;
}

class Background extends React.Component<Props> {
  render(): React.ReactNode {
    return (
      <div className="Background">
        {this.props.useImage && <img src={logo} alt="logo" />}
      </div>
    );
  }
}

export default Background;
