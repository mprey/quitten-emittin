import React from 'react';
import {
  Form,
  InputGroup,
  FormControl,
  Navbar,
  Button,
  Row,
  Dropdown
} from 'react-bootstrap';
import { Filter } from '../../types';
import ModelHeaderListItem from './ModelHeaderListItem';
import ModelHeaderNumberItem from './ModelHeaderNumberItem';

import './ModelHeader.scss';

export interface Props {
  header: string;
  onSearch: (search: string) => void;
  onSort: (key: string, dir: string) => void;
  onFilter: (key: string, value: string | number) => void;
  filters: Filter[];
  activeFilters: { [key: string]: string | number };
}

class ModelHeader extends React.Component<Props> {
  state = {
    search: ''
  };

  renderDropdownNumber(filter: Filter): React.ReactNode {
    return (
      <ModelHeaderNumberItem
        filterKey={filter.key}
        onFilter={this.props.onFilter}
        value={
          this.props.activeFilters && this.props.activeFilters[filter.key]
            ? this.props.activeFilters[filter.key]
            : undefined
        }
      />
    );
  }

  renderDropdownList(filter: Filter): React.ReactNode {
    if (!filter.values) {
      return null;
    }
    return filter.values
      .sort()
      .map(value => (
        <ModelHeaderListItem
          filterKey={filter.key}
          key={value}
          value={value}
          active={
            this.props.activeFilters &&
            this.props.activeFilters[filter.key] === value
          }
          onFilter={this.props.onFilter}
        />
      ));
  }

  render(): React.ReactNode {
    return (
      <div className="ModelHeader w-100">
        <Navbar className="justify-content-between">
          <Navbar.Brand>
            <h1>{this.props.header}</h1>
          </Navbar.Brand>
          <Form>
            <InputGroup>
              <FormControl
                type="text"
                placeholder="Search..."
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  this.setState({ search: e.target.value })
                }
                onKeyPress={(event: React.KeyboardEvent<HTMLDivElement>) => {
                  if (event.key === 'Enter') {
                    this.props.onSearch(this.state.search);
                  }
                }}
              />
              <InputGroup.Append>
                <Button onClick={() => this.props.onSearch(this.state.search)}>
                  Search
                </Button>
              </InputGroup.Append>
            </InputGroup>
          </Form>
        </Navbar>
        <Row className="filters justify-content-between">
          {this.props.filters.map(filter => (
            <Dropdown key={filter.key}>
              <Dropdown.Toggle variant="light" id="dropdown-basic">
                {filter.display}
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item
                  onClick={() => this.props.onSort(filter.key, 'asc')}
                >
                  Sort Ascending
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => this.props.onSort(filter.key, 'desc')}
                >
                  Sort Descending
                </Dropdown.Item>
                <Dropdown.Divider />
                {filter.type === 'number'
                  ? this.renderDropdownNumber(filter)
                  : this.renderDropdownList(filter)}
              </Dropdown.Menu>
            </Dropdown>
          ))}
        </Row>
      </div>
    );
  }
}

export default ModelHeader;
