import React from 'react';
import { Form } from 'react-bootstrap';

interface Props {
  filterKey: string;
  value: string;
  active: boolean;
  onFilter: (key: string, value: string) => void;
}

class ListItem extends React.Component<Props> {
  render(): React.ReactNode {
    const { value, active, filterKey, onFilter } = this.props;
    return (
      <div className="list-item">
        <Form.Check
          name={value}
          checked={active}
          onChange={() => onFilter(filterKey, value)}
        />
        <span>{value}</span>
      </div>
    );
  }
}

export default ListItem;
