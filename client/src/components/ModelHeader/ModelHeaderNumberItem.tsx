import React from 'react';
import { Form, FormControl, InputGroup, Button } from 'react-bootstrap';

interface Props {
  filterKey: string;
  onFilter: (key: string, value: number) => void;
  value?: string | number;
}

class NumberItem extends React.Component<Props> {
  state = {
    filter: ''
  };

  handleFilter(): void {
    const { filter } = this.state;
    if (!Number(filter)) {
      return;
    }
    this.props.onFilter(this.props.filterKey, Number(filter));
  }

  render(): React.ReactNode {
    return (
      <Form className="number">
        <InputGroup>
          <FormControl
            type="text"
            placeholder={
              this.props.value
                ? String(this.props.value)
                : 'Enter minimum # to filter...'
            }
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              this.setState({ filter: e.target.value })
            }
          />
          <InputGroup.Append>
            <Button onClick={() => this.handleFilter()}>Go</Button>
          </InputGroup.Append>
        </InputGroup>
      </Form>
    );
  }
}

export default NumberItem;
