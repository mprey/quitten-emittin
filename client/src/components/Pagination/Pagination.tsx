import React from 'react';

interface Props {
  lastPage: number;
  onPageChange: (page: number) => void;
  page: number;
}

class Pagination extends React.Component<Props> {
  get canPrevious(): boolean {
    return this.props.page > 1;
  }

  get canNext(): boolean {
    return this.props.page < this.props.lastPage;
  }

  get pagesToRender(): number[] {
    if (this.props.page === 1) {
      return Array.from(
        { length: Math.min(this.props.lastPage, 3) },
        (x, i) => i + 1
      );
    } else if (this.props.page === this.props.lastPage) {
      return [
        this.props.lastPage - 2,
        this.props.lastPage - 1,
        this.props.lastPage
      ];
    }

    return [this.props.page - 1, this.props.page, this.props.page + 1];
  }

  render(): React.ReactNode {
    return (
      <nav className="w-100">
        <ul className="pagination justify-content-center">
          <li className={`page-item ${this.canPrevious ? '' : 'disabled'}`}>
            <a
              className="page-link"
              onClick={(): void | boolean =>
                this.canPrevious && this.props.onPageChange(this.props.page - 1)
              }
              href="#"
            >
              <span aria-hidden="true">&laquo;</span>
            </a>
          </li>
          {this.pagesToRender.map(page => (
            <li
              className={`page-item ${
                this.props.page === page ? 'active' : ''
              }`}
              key={page}
            >
              <a
                className="page-link"
                onClick={(): void => this.props.onPageChange(page)}
                href="#"
              >
                {page}
              </a>
            </li>
          ))}
          <li className={`page-item ${this.canNext ? '' : 'disabled'}`}>
            <a
              className="page-link"
              onClick={(): void | boolean =>
                this.canNext && this.props.onPageChange(this.props.page + 1)
              }
              href="#"
            >
              <span aria-hidden="true">&raquo;</span>
            </a>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Pagination;
