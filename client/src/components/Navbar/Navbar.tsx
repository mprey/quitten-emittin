import React from 'react';
import { Navbar, Container, Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';
import logo from '../../static/logo.svg';

import './Navbar.scss';

class NavbarComponent extends React.Component {
  state = {
    isTop: true
  };

  componentDidMount(): void {
    document.addEventListener('scroll', () => {
      const isTop = window.scrollY < 100;
      if (isTop !== this.state.isTop) {
        this.setState({ isTop });
      }
    });
  }

  render(): React.ReactNode {
    return (
      <Navbar
        expand="lg"
        fixed="top"
        className={this.state.isTop ? 'MainNav' : 'MainNav background'}
      >
        <Container>
          <LinkContainer to="/" activeClassName="">
            <Navbar.Brand>
              <img src={logo} style={{ height: '25px' }} alt="logo" />
              Quittin&apos; Emittin&apos;
            </Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ml-auto">
              <NavLink className="nav-link" exact to="/emissions">
                Emissions
              </NavLink>
              <NavLink className="nav-link" exact to="/laws">
                Laws
              </NavLink>
              <NavLink className="nav-link" exact to="/states">
                States
              </NavLink>
              <NavLink className="nav-link" exact to="/visualizations">
                Visualizations
              </NavLink>
              <NavLink className="nav-link" exact to="/other">
                Other
              </NavLink>
              <NavLink className="nav-link" exact to="/about">
                About
              </NavLink>
              <Nav.Link
                href="https://documenter.getpostman.com/view/570117/SVtR1A1J?version=latest"
                target="_blank"
              >
                API
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    );
  }
}

export default NavbarComponent;
