import React from 'react';
import { Container } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

import './Footer.scss';

const links = [
  {
    title: 'Home',
    url: '/'
  },
  {
    title: 'Emissions',
    url: '/emissions'
  },
  {
    title: 'Laws',
    url: '/laws'
  },
  {
    title: 'States',
    url: '/states'
  },
  {
    title: 'About',
    url: '/about'
  }
];

class Footer extends React.Component {
  render(): React.ReactNode {
    return (
      <footer>
        <Container className="Footer">
          <p>&copy; Quittin&apos; Emittin&apos; 2019. All Rights Reserved.</p>
          <ul className="list-inline">
            {links.map(link => (
              <li className="list-inline-item" key={link.url}>
                <LinkContainer to={link.url}>
                  <a>{link.title}</a>
                </LinkContainer>
              </li>
            ))}
            <li className="list-inline-item">
              <a
                href="https://documenter.getpostman.com/view/570117/SVtR1A1J?version=latest"
                target="_blank"
                rel="noopener noreferrer"
              >
                API
              </a>
            </li>
          </ul>
        </Container>
      </footer>
    );
  }
}

export default Footer;
