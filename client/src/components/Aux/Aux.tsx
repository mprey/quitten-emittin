import React from 'react';

const Aux: React.FC<any> = props => {
  return props.children;
};

export default Aux;
