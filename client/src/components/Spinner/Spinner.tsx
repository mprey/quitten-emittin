import React from 'react';

import './Spinner.scss';

class Spinner extends React.Component {
  render(): React.ReactNode {
    return (
      <div className="spinner-wrapper d-flex justify-content-center align-items-center">
        <div className="spinner-grow text-tertiary" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  }
}

export default Spinner;
