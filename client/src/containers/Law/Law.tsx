import React from 'react';
import { Background, Spinner, Aux } from '../../components';
import { RouteComponentProps } from 'react-router';
import {
  Container,
  Row,
  Col,
  Card,
  ListGroup,
  ListGroupItem
} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { connect } from 'react-redux';
import { AppState } from '../../reducers';
import { LawsInstanceState } from '../../types';
import { loadLawsInstance } from '../../actions';

import './Law.scss';

interface MatchParams {
  id: string;
}

export interface Props extends RouteComponentProps<MatchParams> {
  laws: LawsInstanceState;
  loadLawsInstance: typeof loadLawsInstance;
}

class Law extends React.Component<Props> {
  componentWillMount(): void {
    if (
      !this.props.laws.loaded ||
      !this.props.laws.data ||
      this.props.laws.data.id !== this.props.match.params.id
    ) {
      this.props.loadLawsInstance(this.props.match.params.id);
    }
  }

  renderBody(): React.ReactNode {
    if (this.props.laws.loading) {
      return <Spinner />;
    } else if (!this.props.laws.data) {
      return <h1>No data found for ID {this.props.match.params.id}</h1>;
    }

    const law = this.props.laws.data;

    return (
      <Aux>
        <Row className="w-100 justify-content-center">
          <h1>
            <a target="_blank" rel="noopener noreferrer" href={law.url}>
              {law.law_name}
            </a>
          </h1>
          <br />
          <p className="Description">{law.description}</p>
        </Row>
        <Row className="w-100">
          <Col md={6}>
            <h1>Related Emissions</h1>
            <Card>
              <Card.Body>
                <Card.Title>Emissions</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">
                  List of emissions this law is attempting to prevent
                </Card.Subtitle>
                <ListGroup>
                  {law.emissions.map(emission => (
                    <ListGroupItem key={emission}>
                      <LinkContainer to={`/emissions/${emission}`}>
                        <a className="link">{emission}</a>
                      </LinkContainer>
                    </ListGroupItem>
                  ))}
                </ListGroup>
              </Card.Body>
            </Card>
          </Col>
          <Col md={6}>
            <Card className="blue">
              <Card.Header>States Sponsored</Card.Header>
              <Card.Body>
                <LinkContainer to={`/states/${law.state_name}`}>
                  <a>{law.state_name}</a>
                </LinkContainer>
              </Card.Body>
            </Card>
            <Card style={{ marginTop: '20px' }} className="blue">
              <Card.Header>Representatives</Card.Header>
              <Card.Body>{law.representatives}</Card.Body>
            </Card>
          </Col>
        </Row>
      </Aux>
    );
  }

  render(): React.ReactNode {
    return (
      <div>
        <Background useImage={true} />
        <Container className="Content__Container Law">
          <Row className="Cards">{this.renderBody()}</Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState): { laws: LawsInstanceState } => ({
  laws: state.laws.instance
});

const mapDispatchToProps = {
  loadLawsInstance
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Law);
