import React from 'react';
import { Container, Row, Col, Table } from 'react-bootstrap';
import { Background, Spinner, Aux } from '../../components';
import { LinkContainer } from 'react-router-bootstrap';
import { RouteComponentProps } from 'react-router';
import { connect } from 'react-redux';
import { AppState } from '../../reducers';
import { StatesInstanceState } from '../../types';
import { loadStatesInstance } from '../../actions';
import { fixText } from '../../util/text';

import './State.scss';

interface MatchParams {
  name: string;
}

export interface Props extends RouteComponentProps<MatchParams> {
  states: StatesInstanceState;
  loadStatesInstance: typeof loadStatesInstance;
}

class State extends React.Component<Props> {
  componentWillMount(): void {
    if (
      !this.props.states.loaded ||
      !this.props.states.data ||
      this.props.states.data.state_name !== this.props.match.params.name
    ) {
      this.props.loadStatesInstance(this.props.match.params.name);
    }
  }

  renderBody(): React.ReactNode {
    if (this.props.states.loading) {
      return <Spinner />;
    } else if (!this.props.states.data) {
      return <h1>No data found for {this.props.match.params.name}</h1>;
    }

    const state = this.props.states.data;
    const latestSorted = Object.keys(state.emissions).sort(
      (a, b) => state.emissions[b]['2016'] - state.emissions[a]['2016']
    );
    const latestTotal = Object.keys(state.emissions).reduce(
      (value, current) => value + state.emissions[current]['2016'],
      0
    );

    return (
      <Aux>
        <Row>
          <Col md={6}>
            <img src={state.flag_url} alt="state" className="State__Img" />
            <h1>{state.state_name}</h1>
          </Col>
          <Col md={6}>
            <Table striped bordered hover variant="dark" responsive>
              <tbody>
                <tr>
                  <td>Name</td>
                  <td>{state.state_name}</td>
                </tr>
                <tr>
                  <td>Population</td>
                  <td>{state.population}</td>
                </tr>
                <tr>
                  <td>Emissions in 2016</td>
                  <td>{latestTotal} million metric tons</td>
                </tr>
                <tr>
                  <td>Largest Contributor</td>
                  <td>
                    <LinkContainer to={`/emissions/${latestSorted[0]}`}>
                      <a>{latestSorted[0]}</a>
                    </LinkContainer>
                  </td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
        <Row className="w-100">
          <Col md={6}>
            <h1>Emissions:</h1>
            <Table striped bordered hover variant="dark" responsive>
              <thead>
                <tr>
                  <th>Rank</th>
                  <th>Emission</th>
                  <th>Percent Make-up</th>
                </tr>
              </thead>
              <tbody>
                {latestSorted.map((emission, i) => (
                  <tr key={emission}>
                    <td>{i + 1}</td>
                    <td>
                      <LinkContainer to={`/emissions/${emission}`}>
                        <a>{emission}</a>
                      </LinkContainer>
                    </td>
                    <td>
                      {Number(
                        (state.emissions[emission]['2016'] / latestTotal) * 100
                      ).toFixed(2)}
                      %
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
          <Col md={6}>
            <h1>Legislation:</h1>
            <Table
              striped
              bordered
              hover
              variant="dark"
              responsive
              className="text-left"
            >
              <thead>
                <tr>
                  <th>Law ID</th>
                  <th>Legislation</th>
                </tr>
              </thead>
              <tbody>
                {state.legislation.map(l => (
                  <tr key={l.id}>
                    <td style={{ width: '16.66%' }}>
                      <LinkContainer to={`/laws/${l.id}`}>
                        <a>{l.id}</a>
                      </LinkContainer>
                    </td>
                    <td>{fixText(l.name, 150)}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Aux>
    );
  }

  render(): React.ReactNode {
    return (
      <div>
        <Background useImage={true} />
        <Container className="Content__Container State">
          <Row className="Cards">{this.renderBody()}</Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState): { states: StatesInstanceState } => ({
  states: state.states.instance
});

const mapDispatchToProps = {
  loadStatesInstance
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(State);
