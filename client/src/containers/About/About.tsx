import React from 'react';
import { Background, Spinner } from '../../components';
import {
  Container,
  Row,
  CardDeck,
  Card,
  ListGroup,
  ListGroupItem,
  Col
} from 'react-bootstrap';
import { AppState } from '../../reducers';
import { GitLabState } from '../../types';
import { loadGitLabStatistics } from '../../actions';
import { connect } from 'react-redux';
import mprey from '../../static/members/mprey.jpg';
import nidhin from '../../static/members/nidhin.jpeg';
import allen from '../../static/members/allen.jpeg';
import varun from '../../static/members/varun.jpg';
import mark from '../../static/members/mark.jpg';
import jack from '../../static/members/jack.jpeg';
import aws from '../../static/tools/aws.png';
import discord from '../../static/tools/discord.png';
import gitlab from '../../static/tools/gitlab.png';
import namecheap from '../../static/tools/namecheap.png';
import postman from '../../static/tools/postman.png';
import react from '../../static/tools/react.png';

import './About.scss';

const teamMembers = [
  {
    username: 'mprey',
    name: 'Mason Prey',
    role: 'Full-Stack',
    emails: ['masonprey7@gmail.com'],
    image: mprey,
    description:
      'I am a junior CS student. Some of my favorite things to do are exercising, intramural sports, and video games. Programming is also sometimes fun.',
    commits: 0,
    issues: 0,
    tests: 0
  },
  {
    username: 'nidhinbv',
    name: 'Nidhin Varghese',
    role: 'Front-End',
    emails: ['nidhinbv@utexas.edu'],
    image: nidhin,
    description:
      "I'm a senior that would like more experience working with full stack development. In my free time, I like running and learning new recipes.",
    commits: 0,
    issues: 0,
    tests: 0
  },
  {
    username: 'jackrentz',
    name: 'Jack Rentz',
    role: 'Back-End',
    emails: ['johnrentz@Jacks-MacBook-Pro-2.local', 'jackrentz@utexas.edu'],
    image: jack,
    description:
      'I am a senior majoring in Computer Science and minoring in Business.  Some of my hobbies include 3D printing, playing video games, and trying my hand at baking.',
    commits: 0,
    issues: 0,
    tests: 0
  },
  {
    username: 'mmellis',
    name: 'Mark Ellis',
    role: 'Back-End',
    emails: [
      'mark.m.ellis@utexas.edu',
      'Yingjie@wireless-10-147-20-255.public.utexas.edu',
      'Yingjie@wireless-10-147-87-17.public.utexas.edu',
      'Yingjie@wireless-10-147-152-54.public.utexas.edu',
      'Yingjie@wireless-10-147-37-175.public.utexas.edu',
      'Yingjie@wireless-10-147-235-4.public.utexas.edu',
      'Yingjie@wireless-10-147-98-50.public.utexas.edu',
      'Yingjie@wireless-10-147-149-219.public.utexas.edu',
      'Yingjie@wireless-10-147-100-128.public.utexas.edu'
    ],
    image: mark,
    description:
      "Hi, I'm Mark. I am from Michigan currently attending UT Austin as a senior.",
    commits: 0,
    issues: 0,
    tests: 0
  },
  {
    username: 'varunrajaram',
    name: 'Varun Rajaram',
    role: 'Front-End',
    emails: ['varun.rajaram@utexas.edu'],
    image: varun,
    description:
      "I'm Varun, and I'm a junior CS major with a passion for biking, soccer, and the occasional good book.",
    commits: 0,
    issues: 0,
    tests: 0
  },
  {
    username: 'aykzhang',
    name: 'Allen Zhang',
    role: 'Back-End',
    emails: ['azhang@cs.utexas.edu', 'allenzhang42@gmail.com'],
    image: allen,
    description:
      "I'm Allen, and I am currently a senior at UT Austin. I got a dog back home named Sebastian, who acts more like a monkey than anything.",
    commits: 0,
    issues: 0,
    tests: 0
  }
];

interface Props {
  gitlab: GitLabState;
  loadGitLabStatistics: typeof loadGitLabStatistics;
}

class About extends React.Component<Props> {
  state = {
    members: teamMembers
  };

  componentWillMount(): void {
    if (!this.props.gitlab.loaded) {
      this.props.loadGitLabStatistics();
    }
  }

  componentWillReceiveProps(nextProps: Props): void {
    if (this.props.gitlab.commits !== nextProps.gitlab.commits) {
      this.updateMembersState(nextProps);
    }
  }

  updateMembersState(nextProps: Props): void {
    const membersClone = [...this.state.members];

    // commits first
    for (const commit of nextProps.gitlab.commits) {
      const member = membersClone.find(member =>
        member.emails.includes(commit.author_email)
      );
      if (member) {
        member['commits']++;
      }
    }

    // issues second
    for (const issue of nextProps.gitlab.issues.filter(
      issue => issue.closed_by
    )) {
      const member = membersClone.find(
        member => member.name === issue.closed_by.name
      );
      if (member) {
        member['issues']++;
      }
    }

    // tests last
    for (const test of nextProps.gitlab.tests) {
      const member = membersClone.find(
        member => test.username === member.username
      );
      if (member) {
        member['tests'] = test.tests;
      }
    }

    this.setState({ members: membersClone });
  }

  renderMembers(): React.ReactNode {
    if (!this.props.gitlab.loaded || this.props.gitlab.loading) {
      return <Spinner />;
    }

    return (
      <CardDeck>
        {this.state.members.map(member => (
          <Card
            style={{ width: '17rem', minWidth: '13rem' }}
            className="Member"
            key={member.name}
          >
            <Card.Img
              variant="top"
              src={member.image}
              style={{ height: '13rem', width: '100%', objectFit: 'cover' }}
            />
            <Card.Body>
              <Card.Title>{member.name}</Card.Title>
              <Card.Text>{member.role}</Card.Text>
              <Card.Text style={{ fontSize: '12px' }}>
                {member.description}
              </Card.Text>
            </Card.Body>
            <ListGroup className="list-group-flush">
              <ListGroupItem>
                <span className="font-weight-bold">Commits: </span>{' '}
                {member.commits}
              </ListGroupItem>
              <ListGroupItem>
                <span className="font-weight-bold">Issues: </span>{' '}
                {member.issues}
              </ListGroupItem>
              <ListGroupItem>
                <span className="font-weight-bold">Tests: </span> {member.tests}
              </ListGroupItem>
            </ListGroup>
          </Card>
        ))}
      </CardDeck>
    );
  }

  render(): React.ReactNode {
    return (
      <div>
        <Background useImage={true} />
        <Container className="Content__Container About">
          <Row className="Cards">
            <Row className="w-100 justify-content-center padding">
              <h1 className="w-100">Our Mission</h1>
              <p className="Description">
                Our mission is to educate the population about what is currently
                being done legislatively about climate so that we can work
                together to help reverse its catastrophic effects. We want to
                better understand a state by state breakdown of emissions to
                help create an intelligent, and targeted strategy for curbing
                climate change. It&apos;s no longer enough to just be aware of
                climate change. The time has come for us to unite as a society
                and demand comprehensive government action to put an end to the
                rampant and devastating force of climate change through both
                individual action and meaningful legislative reform.
              </p>
            </Row>
            <Row className="w-100 justify-content-center padding">
              <h1 className="w-100">Meet the Team</h1>
              {this.renderMembers()}
            </Row>
            {/* <Row className="w-100 justify-content-center padding Links">
              <h1 className="w-100">Find Out More</h1>
              <Col md={4}>
                <img
                  src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/NASA_logo.svg/1200px-NASA_logo.svg.png"
                  alt="nasa"
                />
                <h4 className="w-100">
                  <a
                    href="https://climate.nasa.gov/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Climate Change at NASA
                  </a>
                </h4>
              </Col>
              <Col md={4}>
                <img
                  src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Flag_of_the_United_Nations.svg/1200px-Flag_of_the_United_Nations.svg.png"
                  alt="un"
                />
                <h4 className="w-100">
                  <a
                    href="https://www.un.org/sustainabledevelopment/climate-change/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    United Nations Sustainable Development
                  </a>
                </h4>
              </Col>
              <Col md={4}>
                <img
                  src="http://cotaporg.wpengine.netdna-cdn.com/wp-content/uploads/2013/03/COTAP-car-magnet.jpg"
                  alt="cotap"
                />
                <h4 className="w-100">
                  <a
                    href="https://cotap.org/reduce-carbon-footprint/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Reducing Your Carbon Footprint
                  </a>
                </h4>
              </Col>
            </Row> */}
            <Row className="w-100 justify-content-center padding Links">
              <h1 className="w-100">Data Sources</h1>
              <Col md={4}>
                <img
                  src="https://static.thenounproject.com/png/286709-200.png"
                  alt="propublica"
                  className="no-radius"
                />
                <h4 className="w-100">
                  <a
                    href="https://projects.propublica.org/api-docs/congress-api/bills/#bills"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    ProPublica Congress API
                  </a>
                </h4>
              </Col>
              <Col md={4}>
                <img
                  src="https://pbs.twimg.com/profile_images/489460437214171136/x9Ca1CfZ_400x400.jpeg"
                  alt="eia"
                />
                <h4 className="w-100">
                  <a
                    href="https://www.eia.gov/environment/emissions/state/analysis/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    US Energy Information Administration
                  </a>
                </h4>
              </Col>
              <Col md={4}>
                <img
                  src="https://images-na.ssl-images-amazon.com/images/I/31d4jDm3UjL.jpg"
                  alt="openstates"
                />
                <h4 className="w-100">
                  <a
                    href="https://docs.openstates.org/en/latest/api/v2/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Open States API
                  </a>
                </h4>
              </Col>
            </Row>
            <Row className="w-100 justify-content-center padding Links">
              <h1 className="w-100">Tools Used</h1>
              <Col md={4}>
                <img src={aws} alt="aws" />
                <h4 className="w-100">
                  <a
                    href="https://aws.amazon.com/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    AWS and Cloudfront
                  </a>
                </h4>
              </Col>
              <Col md={4}>
                <img src={discord} alt="discord" className="no-radius" />
                <h4 className="w-100">
                  <a
                    href="https://discordapp.com/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Discord
                  </a>
                </h4>
              </Col>
              <Col md={4}>
                <img src={gitlab} alt="gitlab" />
                <h4 className="w-100">
                  <a
                    href="https://about.gitlab.com/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Gitlab
                  </a>
                </h4>
              </Col>
            </Row>
            <Row className="w-100 justify-content-center padding Links">
              <Col md={4}>
                <img src={react} alt="react" />
                <h4 className="w-100">
                  <a
                    href="https://reactjs.org/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    React
                  </a>
                </h4>
              </Col>
              <Col md={4}>
                <img src={postman} alt="postman" className="no-radius" />
                <h4 className="w-100">
                  <a
                    href="getpostman.com"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Postman
                  </a>
                </h4>
              </Col>
              <Col md={4}>
                <img src={namecheap} alt="namecheap" className="no-radius" />
                <h4 className="w-100">
                  <a
                    href="https://www.namecheap.com/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Namecheap
                  </a>
                </h4>
              </Col>
            </Row>
            <Row className="w-100 justify-content-center padding Links">
              <h1 className="w-100">External Links</h1>
              <Col md={2}>
                <a
                  href="https://gitlab.com/mprey/quitten-emittin"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  GitLab Repo
                </a>
              </Col>
              <Col md={2}>
                <a
                  href="https://documenter.getpostman.com/view/570117/SVtR1A1J?version=latest"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Postman Docs
                </a>
              </Col>
            </Row>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState): { gitlab: GitLabState } => ({
  gitlab: state.gitlab
});

const mapDispatchToProps = {
  loadGitLabStatistics
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(About);
