import React from 'react';
import { Container, Row, Table } from 'react-bootstrap';
import {
  Background,
  Spinner,
  Pagination,
  Aux,
  ModelHeader
} from '../../components';
import { RouteComponentProps } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import { connect } from 'react-redux';
import { AppState } from '../../reducers';
import { LawsModelState, Filter } from '../../types';
import { loadLawsModel } from '../../actions';
import { fixText } from '../../util/text';

import './Laws.scss';

const TEXT_LIMIT = 100;

export interface Props extends RouteComponentProps {
  laws: LawsModelState;
  loadLawsModel: typeof loadLawsModel;
}

const lawsFilters: Filter[] = [
  {
    key: 'law_name',
    display: 'Law Name',
    type: 'string'
  },
  {
    key: 'creation_date',
    display: 'Creation Date',
    type: 'string'
  },
  {
    key: 'state_name',
    display: 'State Name',
    type: 'string'
  },
  {
    key: 'representatives',
    display: 'Representatives',
    type: 'string'
  },
  {
    key: 'emissions',
    display: 'Emissions',
    type: 'string'
  }
];

class Laws extends React.Component<Props> {
  componentDidMount(): void {
    if (!this.props.laws.loaded) {
      this.props.loadLawsModel(1);
    }
  }

  onPageChange(page: number): void {
    const { filters, sortName, sortDir } = this.props.laws.args;
    this.props.loadLawsModel(page, sortName, sortDir, filters);
  }

  onSearch(search: string): void {
    this.props.history.push(`/search/laws/${search}`);
  }

  onSort(key: string, dir: string): void {
    const { filters } = this.props.laws.args;
    this.props.loadLawsModel(1, key, dir, filters);
  }

  onFilter(key: string, value: string | number) {
    const { filters, sortName, sortDir } = this.props.laws.args;
    if (filters[key] === value) {
      delete filters[key];
    } else {
      filters[key] = value;
    }
    this.props.loadLawsModel(1, sortName, sortDir, filters);
  }

  get filters(): Filter[] {
    return lawsFilters.map(law => ({
      ...law,
      values: this.props.laws.filters[law.key]
    }));
  }

  render(): React.ReactNode {
    return (
      <div>
        <Background useImage={true} />
        <Container className="Content__Container Laws__Container">
          <Row>
            {this.props.laws.loading || !this.props.laws.loaded ? (
              <Spinner />
            ) : (
              <Aux>
                <ModelHeader
                  header="Laws"
                  onSearch={search => this.onSearch(search)}
                  onSort={(key, desc) => this.onSort(key, desc)}
                  onFilter={(key, value) => this.onFilter(key, value)}
                  filters={this.filters}
                  activeFilters={this.props.laws.args.filters}
                />
                <Table striped bordered hover variant="dark" responsive>
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>State</th>
                      <th>Date</th>
                      <th>Related Emissions</th>
                      <th>Representatives</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.props.laws.data.map(law => (
                      <tr key={law.id}>
                        <LinkContainer
                          to={`/laws/${law.id}`}
                          activeClassName=""
                          style={{ width: '41.666%' }}
                        >
                          <td>
                            <div>{fixText(law.law_name, TEXT_LIMIT)}</div>
                          </td>
                        </LinkContainer>
                        <td style={{ width: '8.33%' }}>
                          <div>
                            <LinkContainer to={`/states/${law.state_name}`}>
                              <a href="#">{law.state_name}</a>
                            </LinkContainer>
                          </div>
                        </td>
                        <LinkContainer
                          to={`/laws/${law.id}`}
                          activeClassName=""
                          style={{ width: '8.33%' }}
                        >
                          <td>
                            <div>{law.creation_date}</div>
                          </td>
                        </LinkContainer>

                        <LinkContainer
                          to={`/laws/${law.id}`}
                          activeClassName=""
                          style={{ width: '25%' }}
                        >
                          <td>
                            <div>
                              {law.emissions
                                .map(str => str.trim())
                                .map((emission, index) => (
                                  <Aux key={emission}>
                                    <LinkContainer
                                      to={`/emissions/${emission}`}
                                    >
                                      <a href="#">{emission}</a>
                                    </LinkContainer>
                                    {index === law.emissions.length - 1
                                      ? ''
                                      : ', '}
                                  </Aux>
                                ))}
                            </div>
                          </td>
                        </LinkContainer>

                        <LinkContainer
                          to={`/laws/${law.id}`}
                          activeClassName=""
                          style={{ width: '16.666%' }}
                        >
                          <td>
                            <div>
                              {fixText(
                                law.representatives
                                  .map(
                                    rep =>
                                      rep.charAt(0).toUpperCase() +
                                      rep.toLowerCase().substring(1)
                                  )
                                  .join(', '),
                                TEXT_LIMIT
                              )}
                            </div>
                          </td>
                        </LinkContainer>
                      </tr>
                    ))}
                  </tbody>
                </Table>
                <Pagination
                  page={this.props.laws.args.page}
                  lastPage={this.props.laws.args.lastPage}
                  onPageChange={(page: number): void => this.onPageChange(page)}
                />
              </Aux>
            )}
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState): { laws: LawsModelState } => ({
  laws: state.laws.model
});

const mapDispatchToProps = {
  loadLawsModel
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Laws);
