import React, { RefObject } from 'react';
import { Background, Spinner } from '../../components';
import { Container, Row, Tab, Tabs } from 'react-bootstrap';
import BubbleChart from '@weknow/react-bubble-chart-d3';
import statesPaths from './states';
import PieChart from './PieChart';
import * as d3 from 'd3';
import stateMapData from './data/state_map.json';
import pieChartData from './data/pie_chart.json';
import bubbleChartData from './data/bubble_chart.json';

import './Visualizations.scss';

class Visualizations extends React.Component {
  visualWrapper: RefObject<HTMLDivElement>;
  state = {
    key: 'state'
  };

  constructor() {
    super({});

    this.visualWrapper = React.createRef();
  }

  renderPieChart(): React.ReactNode {
    return (
      <PieChart
        width={500}
        height={500}
        data={Object.keys(pieChartData).map(key => ({
          label: key,
          value: pieChartData[key]
        }))}
      />
    );
  }

  renderStateMap(): React.ReactNode {
    const max =
      stateMapData[
        Object.keys(stateMapData).sort(
          (a, b) => stateMapData[b] - stateMapData[a]
        )[0]
      ];

    const data = Object.keys(stateMapData).reduce((prev, curr) => {
      prev[curr] = {
        total: stateMapData[curr],
        color: d3.interpolate('#abd1ea', '#110644')(stateMapData[curr] / max)
      };
      return prev;
    }, {});

    setTimeout(() => {
      function toolTip(title: string, total: string): string {
        return `<h3>${title}</h3><h4>Num reps: ${total}</h4>`;
      }

      function mouseOver(d: { id: string; n: string; d: string }) {
        d3.select('#tooltip')
          .transition()
          .duration(200)
          .style('opacity', 0.9);

        d3.select('#tooltip')
          .html(
            toolTip(
              d.n,
              data[d.id] ? `${data[d.id].total} representatives` : 'No Data'
            )
          )
          .style('left', d3.event.pageX - 100 + 'px')
          .style('top', d3.event.pageY - 28 + 'px');
      }

      function mouseOut() {
        d3.select('#tooltip')
          .transition()
          .duration(500)
          .style('opacity', 0);
      }

      d3.select('#statesvg')
        .selectAll('.state')
        .data(statesPaths)
        .enter()
        .append('path')
        .attr('class', 'state')
        .attr('d', function(d) {
          return d.d;
        })
        .style('fill', function(d) {
          return data[d.id] ? data[d.id].color : '#ffffff';
        })
        .on('mouseover', mouseOver)
        .on('mouseout', mouseOut);
    }, 1);

    const width = this.visualWrapper.current
      ? this.visualWrapper.current.offsetWidth
      : 1100;
    return (
      <div className="container">
        <div id="tooltip"></div>
        <svg width={'100%'} height={'1100px'} id="statesvg"></svg>
      </div>
    );
  }

  renderBubbleChart(): React.ReactNode {
    const width = this.visualWrapper.current
      ? this.visualWrapper.current.offsetWidth
      : 500;

    return (
      <BubbleChart
        bubbleClickFun={console.log}
        graph={{
          zoom: 1.0,
          offsetX: 0.0,
          offsetY: 0.0
        }}
        showLegend={false}
        width={width - 300}
        height={width - 300}
        padding={0}
        valueFont={{
          family: 'Arial',
          size: 12,
          color: '#fff',
          weight: 'bold',
          align: 'center'
        }}
        labelFont={{
          family: 'Arial',
          size: 12,
          color: '#fff',
          weight: 'bold',
          align: 'center'
        }}
        data={Object.keys(bubbleChartData).map(key => ({
          label: key,
          value: bubbleChartData[key]
        }))}
      />
    );
  }

  render(): React.ReactNode {
    return (
      <div>
        <Background useImage={true} />
        <Container className="Content__Container Visualizations">
          <div ref={this.visualWrapper}>
            <Row className="Cards">
              <Tabs
                id="tabs"
                activeKey={this.state.key}
                onSelect={(key: string): void => this.setState({ key })}
              >
                <Tab eventKey="state" title="State Map">
                  <h1>Number of representatives per state</h1>
                  {this.renderStateMap()}
                </Tab>
                <Tab eventKey="bubble" title="Bubble Chart">
                  <h1>Bubble chart of number of representatives by issue</h1>
                  {this.renderBubbleChart()}
                </Tab>
                <Tab eventKey="pie" title="Pie Chart">
                  <h1>Pie chart of total representatives by party</h1>
                  {this.renderPieChart()}
                </Tab>
              </Tabs>
            </Row>
          </div>
        </Container>
      </div>
    );
  }
}

export default Visualizations;
