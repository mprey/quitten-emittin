import React from 'react';
import * as d3 from 'd3';
import { Aux } from '../../components';

function translate(x: number, y: number): string {
  return `translate(${x}, ${y})`;
}

export interface Props {
  width: number;
  height: number;
  data: { label: string; value: number }[];
}

interface SliceProps {
  innerRadius: number;
  outerRadius: number;
  cornerRadius: number;
  padAngle: number;
  value: d3.PieArcDatum<number | { valueOf(): number }>;
  label: string;
  fill: string;
}

class Slice extends React.Component<SliceProps> {
  state = {
    hover: false
  };

  render(): React.ReactNode {
    const {
      value,
      label,
      fill,
      innerRadius,
      cornerRadius,
      padAngle,
      ...props
    } = this.props;
    let { outerRadius } = this.props;
    if (this.state.hover) {
      outerRadius *= 1.1;
    }
    const arc = d3
      .arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius)
      .cornerRadius(cornerRadius)
      .padAngle(padAngle);
    return (
      <Aux>
        <g
          onMouseOver={() => this.setState({ hover: true })}
          onMouseOut={() => this.setState({ hover: false })}
          {...props}
        >
          <path
            d={arc({ ...value, outerRadius, innerRadius }) || undefined}
            fill={fill}
          />
          {this.state.hover && (
            <Aux>
              <text transform={translate(0, 0)} dy=".35em" className="label">
                {label}
              </text>
              <text transform={translate(0, 15)} dy=".35em" className="label">
                {value.data.valueOf()} representatives
              </text>
            </Aux>
          )}
        </g>
        {this.state.hover && (
          <div className="pie-hover">
            <h3>{label}</h3>
            <h4>{value.data.valueOf()} million metric tons</h4>
          </div>
        )}
      </Aux>
    );
  }
}

export default class PieChart extends React.Component<Props> {
  innerRadius = 0;
  outerRadius = 0;
  cornerRadius = 0;
  padAngle = 0;
  radius = 0;
  x = 0;
  y = 0;
  colorScale = d3.scaleOrdinal(d3.schemeCategory10);

  findValue(value: number | { valueOf(): number }): string {
    if (typeof value === 'object') {
      value = value.valueOf();
    }
    const found = this.props.data.find(i => i.value === value);
    if (found) {
      return found.label;
    }
    return 'No Label';
  }

  renderPie(): React.ReactNode {
    const pie = d3.pie();
    return (
      <g transform={translate(this.x, this.y)}>
        {pie(this.props.data.map(i => i.value)).map((value, i) =>
          this.renderSlice(value, i, this.findValue(value.data))
        )}
      </g>
    );
  }

  renderSlice(
    value: d3.PieArcDatum<number | { valueOf(): number }>,
    index: number,
    label: string
  ): React.ReactNode {
    return (
      <Slice
        key={index}
        innerRadius={this.innerRadius}
        outerRadius={this.outerRadius}
        cornerRadius={this.cornerRadius}
        padAngle={this.padAngle}
        value={value}
        label={label}
        fill={this.colorScale(index + '')}
      />
    );
  }

  render() {
    const { width, height } = this.props;
    this.radius = (Math.min(width, height) * 0.9) / 2;
    this.x = width / 2;
    this.y = height / 2;
    this.innerRadius = this.radius * 0.35;
    this.outerRadius = this.radius;
    this.cornerRadius = 7;
    this.padAngle = 0.02;

    return (
      <svg width={width} height={height}>
        {this.renderPie()}
      </svg>
    );
  }
}
