import React from 'react';
import {
  Card,
  Button,
  Container,
  Row,
  CardDeck,
  ListGroup,
  ListGroupItem
} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import {
  Background,
  Spinner,
  Pagination,
  Aux,
  ModelHeader
} from '../../components';
import { RouteComponentProps } from 'react-router';
import { connect } from 'react-redux';
import { AppState } from '../../reducers';
import { StatesModelState, Filter } from '../../types';
import { loadStatesModel } from '../../actions';

import './States.scss';

export interface Props extends RouteComponentProps {
  states: StatesModelState;
  loadStatesModel: typeof loadStatesModel;
}

const statesFilters: Filter[] = [
  {
    key: 'population',
    display: 'Population',
    type: 'number'
  },
  {
    key: 'num_representatives',
    display: '# of Reps',
    type: 'number'
  },
  {
    key: 'emissions',
    display: 'Emissions',
    type: 'number'
  },
  {
    key: 'largest_contributor',
    display: 'Largest Contributor',
    type: 'string'
  },
  {
    key: 'laws_passed',
    display: '# of Laws',
    type: 'number'
  }
];

class States extends React.Component<Props> {
  componentDidMount(): void {
    if (!this.props.states.loaded) {
      this.props.loadStatesModel(1);
    }
  }

  onPageChange(page: number): void {
    const { filters, sortName, sortDir } = this.props.states.args;
    this.props.loadStatesModel(page, sortName, sortDir, filters);
  }

  onSearch(search: string): void {
    this.props.history.push(`/search/states/${search}`);
  }

  onSort(key: string, dir: string): void {
    const { filters } = this.props.states.args;
    this.props.loadStatesModel(1, key, dir, filters);
  }

  onFilter(key: string, value: string | number) {
    const { filters, sortName, sortDir } = this.props.states.args;
    if (filters[key] === value) {
      delete filters[key];
    } else {
      filters[key] = value;
    }
    this.props.loadStatesModel(1, sortName, sortDir, filters);
  }

  get filters(): Filter[] {
    return statesFilters.map(state => ({
      ...state,
      values: this.props.states.filters[state.key]
    }));
  }

  render(): React.ReactNode {
    return (
      <div>
        <Background useImage={true} />
        <Container className="Content__Container States">
          <Row className="Cards">
            {!this.props.states.loaded || this.props.states.loading ? (
              <Spinner />
            ) : (
              <Aux>
                <CardDeck className="justify-content-center">
                  <ModelHeader
                    header="States"
                    onSearch={search => this.onSearch(search)}
                    onSort={(key, desc) => this.onSort(key, desc)}
                    onFilter={(key, value) => this.onFilter(key, value)}
                    filters={this.filters}
                    activeFilters={this.props.states.args.filters}
                  />
                  {this.props.states.data.map(item => (
                    <Card
                      style={{ maxWidth: '17rem', minWidth: '17rem' }}
                      key={item.state_name}
                      className="State"
                    >
                      <Card.Img
                        variant="top"
                        src={item.flag_url}
                        style={{ height: '190px' }}
                      />
                      <Card.Body>
                        <Card.Title>
                          {item.state_name.replace('_', ' ')}
                        </Card.Title>
                        <ListGroup className="list-group-flush">
                          <ListGroupItem>
                            <span className="font-weight-bold">
                              Population:{' '}
                            </span>{' '}
                            {item.population}
                          </ListGroupItem>
                          <ListGroupItem>
                            <span className="font-weight-bold">
                              Number of Legislatures:{' '}
                            </span>{' '}
                            {item.num_representatives}
                          </ListGroupItem>
                          <ListGroupItem>
                            <span className="font-weight-bold">
                              Emissions:{' '}
                            </span>{' '}
                            {Number(item.emissions).toFixed(2)} mil. metric tons
                          </ListGroupItem>
                          <ListGroupItem>
                            <span className="font-weight-bold">
                              Largest Contributor:{' '}
                            </span>{' '}
                            {item.largest_contributor}
                          </ListGroupItem>
                          <ListGroupItem>
                            <span className="font-weight-bold">
                              Number of Laws:{' '}
                            </span>{' '}
                            {item.laws_passed}
                          </ListGroupItem>
                        </ListGroup>
                        <LinkContainer to={`states/${item.state_name}`}>
                          <Button variant="primary" className="mt">
                            More Info
                          </Button>
                        </LinkContainer>
                      </Card.Body>
                    </Card>
                  ))}
                </CardDeck>
                <Pagination
                  page={this.props.states.args.page}
                  lastPage={this.props.states.args.lastPage}
                  onPageChange={(page: number): void => this.onPageChange(page)}
                />
              </Aux>
            )}
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState): { states: StatesModelState } => ({
  states: state.states.model
});

const mapDispatchToProps = {
  loadStatesModel
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(States);
