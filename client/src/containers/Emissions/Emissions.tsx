import React from 'react';
import {
  Card,
  Button,
  Container,
  Row,
  CardDeck,
  ListGroup,
  ListGroupItem
} from 'react-bootstrap';
import {
  Background,
  Spinner,
  Pagination,
  Aux,
  ModelHeader
} from '../../components';
import { RouteComponentProps } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import { connect } from 'react-redux';
import { AppState } from '../../reducers';
import { EmissionsModelState, Filter } from '../../types';
import { loadEmissionsModel } from '../../actions';

import './Emissions.scss';

export interface Props extends RouteComponentProps {
  emissions: EmissionsModelState;
  loadEmissionsModel: typeof loadEmissionsModel;
}

const emissionsFilters: Filter[] = [
  {
    key: 'total_emissions',
    display: 'Emissions',
    type: 'number'
  },
  {
    key: 'best_state',
    display: 'Best State',
    type: 'string'
  },
  {
    key: 'worst_state',
    display: 'Worst State',
    type: 'string'
  },
  {
    key: 'worst_year',
    display: 'Worst Year',
    type: 'string'
  },
  {
    key: 'laws_passed',
    display: 'Laws Passed',
    type: 'number'
  }
];

class Emissions extends React.Component<Props> {
  componentWillMount(): void {
    if (!this.props.emissions.loaded) {
      this.props.loadEmissionsModel(1);
    }
  }

  onPageChange(page: number): void {
    const { filters, sortName, sortDir } = this.props.emissions.args;
    this.props.loadEmissionsModel(page, sortName, sortDir, filters);
  }

  onSearch(search: string): void {
    this.props.history.push(`/search/emissions/${search}`);
  }

  onSort(key: string, dir: string): void {
    const { filters } = this.props.emissions.args;
    this.props.loadEmissionsModel(1, key, dir, filters);
  }

  onFilter(key: string, value: string | number) {
    const { filters, sortName, sortDir } = this.props.emissions.args;
    if (filters[key] === value) {
      delete filters[key];
    } else {
      filters[key] = value;
    }
    this.props.loadEmissionsModel(1, sortName, sortDir, filters);
  }

  get filters(): Filter[] {
    return emissionsFilters.map(emission => ({
      ...emission,
      values: this.props.emissions.filters[emission.key]
    }));
  }

  render(): React.ReactNode {
    return (
      <div>
        <Background useImage={true} />

        <Container className="Content__Container Emissions">
          <Row className="Cards">
            {!this.props.emissions.loaded || this.props.emissions.loading ? (
              <Spinner />
            ) : (
              <Aux>
                <CardDeck className="justify-content-center">
                  <ModelHeader
                    header="Emissions"
                    onSearch={search => this.onSearch(search)}
                    onSort={(key, desc) => this.onSort(key, desc)}
                    onFilter={(key, value) => this.onFilter(key, value)}
                    filters={this.filters}
                    activeFilters={this.props.emissions.args.filters}
                  />
                  {this.props.emissions.data.map(item => (
                    <Card
                      style={{ maxWidth: '17rem', minWidth: '17rem' }}
                      key={item.emission_name}
                      className="Emission"
                    >
                      <Card.Img
                        variant="top"
                        src={item.emission_pic_url}
                        style={{ height: '190px' }}
                      />
                      <Card.Body>
                        <Card.Title>{item.emission_name}</Card.Title>
                        <ListGroup className="list-group-flush">
                          <ListGroupItem>
                            <span className="font-weight-bold">Total: </span>{' '}
                            {item.total_emissions} million metric tons
                          </ListGroupItem>
                          <ListGroupItem>
                            <span className="font-weight-bold">
                              Best State:{' '}
                            </span>{' '}
                            {item.best_state}
                          </ListGroupItem>
                          <ListGroupItem>
                            <span className="font-weight-bold">
                              Worst State:{' '}
                            </span>{' '}
                            {item.worst_state}
                          </ListGroupItem>
                          <ListGroupItem>
                            <span className="font-weight-bold">
                              Worst Year:{' '}
                            </span>{' '}
                            {item.worst_year}
                          </ListGroupItem>
                          <ListGroupItem>
                            <span className="font-weight-bold">
                              Laws Passed:{' '}
                            </span>{' '}
                            {item.laws_passed}
                          </ListGroupItem>
                        </ListGroup>
                        <LinkContainer to={`/emissions/${item.emission_name}`}>
                          <Button variant="primary" className="mt">
                            More Info{' '}
                          </Button>
                        </LinkContainer>
                      </Card.Body>
                    </Card>
                  ))}
                </CardDeck>
                <Pagination
                  page={this.props.emissions.args.page}
                  lastPage={this.props.emissions.args.lastPage}
                  onPageChange={(page: number): void => this.onPageChange(page)}
                />
              </Aux>
            )}
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (
  state: AppState
): { emissions: EmissionsModelState } => ({
  emissions: state.emissions.model
});

const mapDispatchToProps = {
  loadEmissionsModel
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Emissions);
