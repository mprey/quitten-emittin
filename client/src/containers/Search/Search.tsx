import React from 'react';
import { RouteComponentProps } from 'react-router';
import { connect } from 'react-redux';
import { SearchState } from '../../types';
import { Background, Aux, Spinner } from '../../components';
import {
  Container,
  Row,
  Tab,
  Tabs,
  Form,
  FormControl,
  InputGroup,
  Button
} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import {
  globalSearch,
  lawSearch,
  stateSearch,
  emissionSearch
} from '../../actions';
import { AppState } from '../../reducers';
import { fixText } from '../../util/text';

import './Search.scss';

const PRECEDING_LENGTH = 50;
const SUCCEEDING_LENGTH = 75;
const TITLE_TEXT_LIMIT = 75;

type RenderData = {
  title: string;
  description: string;
  displayLink: string;
  link: string;
};

interface MatchParams {
  model: string;
  query: string;
}

export interface Props extends RouteComponentProps<MatchParams> {
  globalSearch: typeof globalSearch;
  lawSearch: typeof lawSearch;
  stateSearch: typeof stateSearch;
  emissionSearch: typeof emissionSearch;
  search: SearchState;
}

class Search extends React.Component<Props> {
  state = {
    key: 'global',
    search: ''
  };

  componentWillMount(): void {
    if (
      !this.props.search.loaded ||
      this.props.search.args.search !== this.props.match.params.query
    ) {
      this.loadData(
        this.props.match.params.query,
        this.props.match.params.model
      );
    }

    this.setState({ key: this.props.match.params.model });
  }

  componentWillReceiveProps(nextProps: Props): void {
    if (this.props.match.params.query !== nextProps.match.params.query) {
      this.loadData(nextProps.match.params.query, nextProps.match.params.model);
    }
  }

  isDisabled(key: string): boolean {
    return (
      this.props.match.params.model !== 'global' &&
      this.props.match.params.model !== key
    );
  }

  loadData(query: string, model: string): void {
    switch (model) {
      case 'states':
        this.props.stateSearch(query);
        break;
      case 'emissions':
        this.props.emissionSearch(query);
        break;
      case 'laws':
        this.props.lawSearch(query);
        break;
      case 'global':
        this.props.globalSearch(query);
        break;
    }
  }

  search(): void {
    this.props.history.push(
      `/search/${this.props.match.params.model}/${this.state.search}`
    );
  }

  renderData(key: string): RenderData[] {
    const results: RenderData[] = [];
    const { emissions, laws, states } = this.props.search.data;

    // first, get the emissions data
    if (key === 'global' || key === 'emissions') {
      results.push(
        ...emissions.map(emission => ({
          title: emission.emission_name,
          description: emission.description,
          displayLink: `https://quittinemittin.me/emissions/${emission.emission_name}`,
          link: `/emissions/${emission.emission_name}`
        }))
      );
    }

    // second, laws data
    if (key === 'global' || key === 'laws') {
      results.push(
        ...laws.map(law => ({
          title: law.law_name,
          description: law.description,
          displayLink: `https://quittinemittin.me/laws/${law.id}`,
          link: `/laws/${law.id}`
        }))
      );
    }

    // last, states data
    if (key === 'global' || key === 'states') {
      results.push(
        ...states.map(state => ({
          title: state.state_name,
          description: state.state_name,
          displayLink: `https://quittinemittin.me/states/${state.state_name}`,
          link: `/states/${state.state_name}`
        }))
      );
    }

    return results;
  }

  hightlightData(description: string, query: string): React.ReactNode {
    const index = description.indexOf(query);
    let startIndex = index - PRECEDING_LENGTH;
    let endIndex = index + SUCCEEDING_LENGTH;
    if (startIndex < 0) {
      endIndex += -startIndex;
      startIndex = 0;
    }

    endIndex =
      endIndex < description.length
        ? description.indexOf(' ', endIndex)
        : endIndex;
    const newDescription =
      '...' + description.slice(startIndex, endIndex) + '...';
    const parts = newDescription.split(new RegExp(`(${query})`, 'gi'));
    return (
      <span>
        {parts.map((part, i) => (
          <span
            key={i}
            className={
              part.toLowerCase() === query.toLowerCase() ? 'highlight' : ''
            }
          >
            {part}
          </span>
        ))}
      </span>
    );
  }

  renderBody(key: string): React.ReactNode {
    const { query } = this.props.match.params;
    const renderData = this.renderData(key);
    if (renderData.length === 0) {
      return (
        <Row className="w-100">
          <h2>No results found for &quot;{query}&quot;</h2>
        </Row>
      );
    }

    return (
      <Aux>
        <Row className="w-100">
          <h2>Search results for &quot;{query}&quot;</h2>
          {this.renderData(key).map(item => (
            <Row className="search-item" key={item.link}>
              <LinkContainer to={item.link}>
                <a href="#">{fixText(item.title, TITLE_TEXT_LIMIT)}</a>
              </LinkContainer>
              <p className="link">{item.displayLink}</p>
              <p className="description">
                {this.hightlightData(item.description, query)}
              </p>
            </Row>
          ))}
        </Row>
      </Aux>
    );
  }

  render(): React.ReactNode {
    return (
      <div>
        <Background useImage={true} />
        <Container className="Content__Container Search">
          <Row className="Cards">
            {!this.props.search.loaded || this.props.search.loading ? (
              <Spinner />
            ) : (
              <Aux>
                <Tabs
                  id="tabs"
                  activeKey={this.state.key}
                  onSelect={(key: string): void => this.setState({ key })}
                >
                  <Tab
                    eventKey="global"
                    title="Global"
                    disabled={this.isDisabled('global')}
                  >
                    {this.renderBody('global')}
                  </Tab>
                  <Tab
                    eventKey="emissions"
                    title="Emissions"
                    disabled={this.isDisabled('emissions')}
                  >
                    {this.renderBody('emissions')}
                  </Tab>
                  <Tab
                    eventKey="laws"
                    title="Laws"
                    disabled={this.isDisabled('laws')}
                  >
                    {this.renderBody('laws')}
                  </Tab>
                  <Tab
                    eventKey="states"
                    title="States"
                    disabled={this.isDisabled('states')}
                  >
                    {this.renderBody('states')}
                  </Tab>
                </Tabs>
                <Form>
                  <InputGroup>
                    <FormControl
                      type="text"
                      placeholder="Search..."
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                        this.setState({ search: e.target.value })
                      }
                      onKeyPress={(
                        event: React.KeyboardEvent<HTMLDivElement>
                      ) => {
                        if (event.key === 'Enter') {
                          this.search();
                        }
                      }}
                    />
                    <InputGroup.Append>
                      <Button onClick={() => this.search()}>Search</Button>
                    </InputGroup.Append>
                  </InputGroup>
                </Form>
                {/* <Pagination
                  page={this.props.emissions.args.page}
                  lastPage={this.props.emissions.args.lastPage}
                  onPageChange={(page: number): void => this.onPageChange(page)}
                /> */}
              </Aux>
            )}
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState): { search: SearchState } => ({
  search: state.search
});

const mapDispatchToProps = {
  globalSearch,
  lawSearch,
  stateSearch,
  emissionSearch
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);
