import React from 'react';
import { Background, Spinner, Aux } from '../../components';
import { RouteComponentProps } from 'react-router';
import { Container, Row, Col, Table, Card } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { connect } from 'react-redux';
import { AppState } from '../../reducers';
import { EmissionsInstanceState } from '../../types';
import { loadEmissionsInstance } from '../../actions';
import { fixText } from '../../util/text';

import './Emission.scss';

interface MatchProps {
  name: string;
}

export interface Props extends RouteComponentProps<MatchProps> {
  emissions: EmissionsInstanceState;
  loadEmissionsInstance: typeof loadEmissionsInstance;
}

class Emission extends React.Component<Props> {
  componentWillMount(): void {
    if (
      !this.props.emissions.loaded ||
      !this.props.emissions.data ||
      this.props.emissions.data.emission_name !== this.props.match.params.name
    ) {
      this.props.loadEmissionsInstance(this.props.match.params.name);
    }
  }

  topStates(amount: number, year: number): string[] {
    if (!this.props.emissions.data) return [];
    const emissions = this.props.emissions.data;

    return Object.keys(emissions.states)
      .sort((a, b) => emissions.states[b][year] - emissions.states[a][year])
      .slice(0, amount);
  }

  renderBody(): React.ReactNode {
    if (this.props.emissions.loading) {
      return <Spinner />;
    } else if (!this.props.emissions.data) {
      return <h1>No data found for {this.props.match.params.name}</h1>;
    }

    const emission = this.props.emissions.data;

    return (
      <Aux>
        <Row>
          <Col md={6}>
            <img
              src={emission.emission_pic_url}
              alt="img"
              className="Emission__Img"
            />
            <h1>{emission.emission_name}</h1>
            <p>
              <span className="font-weight-bold">Best State: </span>
              {emission.best_state}
            </p>
            <p>
              <span className="font-weight-bold">Worst State: </span>
              {emission.worst_state}
            </p>
            <p>
              <span className="font-weight-bold">Worst Year: </span>
              {emission.worst_year}
            </p>
            <p>
              <span className="font-weight-bold">Laws Passed: </span>
              {emission.laws_passed}
            </p>
          </Col>
          <Col md={6}>
            <h3>Top Contributors by State</h3>
            <Table striped bordered hover variant="dark" responsive>
              <thead>
                <tr>
                  <th>Rank</th>
                  <th>State</th>
                  <th>Total Emission</th>
                </tr>
              </thead>
              <tbody>
                {this.topStates(5, 2016).map((state, i) => (
                  <tr key={state}>
                    <td>{i + 1}</td>
                    <td>
                      <LinkContainer to={`/states/${state}`}>
                        <a>{state}</a>
                      </LinkContainer>
                    </td>
                    <td>{emission.states[state][2016]} million metric tons</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
        <Row className="w-100">
          <Col md={6}>
            <h1>Total Emissions in 2018:</h1>
            <h4>
              {Number(
                Object.keys(emission.states).reduce(
                  (prev, curr) => prev + emission.states[curr]['2016'],
                  0
                )
              ).toFixed(2)}{' '}
              million metric tons
            </h4>
          </Col>
          <Col md={6}>
            <Card>
              <Card.Header>Get Involved</Card.Header>
              <Card.Body>{emission.tips[0]}</Card.Body>
            </Card>
          </Col>
        </Row>
        <Row className="w-100 justify-content-center">
          <Col md={6}>
            <h3>States by Emission Amount</h3>
            <Table striped bordered hover variant="dark" responsive>
              <thead>
                <tr>
                  <th>Rank</th>
                  <th>State</th>
                  <th>Emissions</th>
                </tr>
              </thead>
              <tbody>
                {this.topStates(51, 2016).map((state, i) => (
                  <tr key={state}>
                    <td>{i + 1}</td>
                    <td>
                      <LinkContainer to={`/states/${state}`}>
                        <a>{state}</a>
                      </LinkContainer>
                    </td>
                    <td>{emission.states[state][2016]} million metric tons</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
          <Col md={6}>
            <h3>Legislation to Combat</h3>
            <Table striped bordered hover variant="dark" responsive>
              <thead>
                <tr>
                  <th>Law ID</th>
                  <th>Legislation</th>
                </tr>
              </thead>
              <tbody>
                {emission.legislation.map(l => (
                  <tr key={l.id}>
                    <td style={{ width: '16.66%' }}>
                      <LinkContainer to={`/laws/${l.id}`}>
                        <a>{l.id}</a>
                      </LinkContainer>
                    </td>
                    <td>{fixText(l.name, 150)}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Aux>
    );
  }

  render(): React.ReactNode {
    return (
      <div>
        <Background useImage={true} />
        <Container className="Content__Container Emission">
          <Row className="Cards">{this.renderBody()}</Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (
  state: AppState
): { emissions: EmissionsInstanceState } => ({
  emissions: state.emissions.instance
});

const mapDispatchToProps = {
  loadEmissionsInstance
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Emission);
