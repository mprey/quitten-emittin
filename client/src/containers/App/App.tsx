import React from 'react';
import { Navbar, Splash, Footer, ScrollToTop } from '../../components';
import {
  States,
  Emissions,
  Laws,
  State,
  Emission,
  Law,
  About,
  Search,
  Visulizations,
  Other
} from '../';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';

import './App.scss';

const App: React.FC = () => {
  return (
    <Router>
      <ScrollToTop>
        <div className="App">
          <div className="App__Content">
            <Navbar />
            <Switch>
              <Route exact path="/about" component={About} />
              <Route exact path="/visualizations" component={Visulizations} />
              <Route exact path="/other" component={Other} />
              <Route exact path="/states/:name" component={State} />
              <Route exact path="/states" component={States} />
              <Route exact path="/emissions/:name" component={Emission} />
              <Route exact path="/emissions" component={Emissions} />
              <Route exact path="/laws/:id" component={Law} />
              <Route exact path="/laws" component={Laws} />
              <Route exact path="/search/:model/:query" component={Search} />
              <Route exact path="/index.html">
                <Redirect to="/" />
              </Route>
              <Route exact path="/" component={Splash} />
            </Switch>
          </div>
          <Footer />
        </div>
      </ScrollToTop>
    </Router>
  );
};

export default App;
