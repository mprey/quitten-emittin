import React, { RefObject } from 'react';
import { Background, Spinner } from '../../components';
import { Container, Row, Tab, Tabs } from 'react-bootstrap';
import { connect } from 'react-redux';
import { AppState } from '../../reducers';
import { VisualState } from '../../types';
import { loadStatesModel, loadEmissionsModel } from '../../actions';
import BubbleChart from '@weknow/react-bubble-chart-d3';
import statesPaths from './states';
import PieChart from './PieChart';
import * as d3 from 'd3';

import './Visualizations.scss';

interface Props {
  visuals: VisualState;
  loadStatesModel: typeof loadStatesModel;
  loadEmissionsModel: typeof loadEmissionsModel;
}

class Visualizations extends React.Component<Props> {
  visualWrapper: RefObject<HTMLDivElement>;
  state = {
    key: 'state'
  };

  constructor(props: Props) {
    super(props);

    this.visualWrapper = React.createRef();
  }

  renderPieChart(): React.ReactNode {
    if (this.props.visuals.pieChart.loading) {
      return <Spinner />;
    } else if (!this.props.visuals.pieChart.loaded) {
      this.props.loadEmissionsModel(1);
      return <Spinner />;
    }

    return (
      <PieChart
        width={500}
        height={500}
        data={this.props.visuals.pieChart.data}
      />
    );
  }

  renderStateMap(): React.ReactNode {
    if (this.props.visuals.stateMap.loading) {
      return <Spinner />;
    } else if (!this.props.visuals.stateMap.loaded) {
      this.props.loadStatesModel(1, undefined, undefined, undefined, 500);
      return <Spinner />;
    }

    const max = this.props.visuals.stateMap.data.sort(
      (a, b) => b.total - a.total
    )[0].total;

    const data = this.props.visuals.stateMap.data.reduce((prev, curr) => {
      prev[curr.id] = {
        total: curr.total,
        color: d3.interpolate('#abd1ea', '#110644')(curr.total / max)
      };
      return prev;
    }, {});

    setTimeout(() => {
      function toolTip(title: string, total: string): string {
        return `<h3>${title}</h3><h4>Total: ${total}</h4>`;
      }

      function mouseOver(d: { id: string; n: string; d: string }) {
        d3.select('#tooltip')
          .transition()
          .duration(200)
          .style('opacity', 0.9);

        d3.select('#tooltip')
          .html(
            toolTip(
              d.n,
              data[d.id]
                ? `${Number(data[d.id].total).toFixed(2)} million metric tons`
                : 'No Data'
            )
          )
          .style('left', d3.event.pageX - 400 + 'px')
          .style('top', d3.event.pageY - 28 + 'px');
      }

      function mouseOut() {
        d3.select('#tooltip')
          .transition()
          .duration(500)
          .style('opacity', 0);
      }

      d3.select('#statesvg')
        .selectAll('.state')
        .data(statesPaths)
        .enter()
        .append('path')
        .attr('class', 'state')
        .attr('d', function(d) {
          return d.d;
        })
        .style('fill', function(d) {
          return data[d.id] ? data[d.id].color : '#ffffff';
        })
        .on('mouseover', mouseOver)
        .on('mouseout', mouseOut);
    }, 1);

    const width = this.visualWrapper.current
      ? this.visualWrapper.current.offsetWidth
      : 1100;
    return (
      <div className="container">
        <div id="tooltip"></div>
        <svg width={'100%'} height={'1100px'} id="statesvg"></svg>
      </div>
    );
  }

  renderBubbleChart(): React.ReactNode {
    if (this.props.visuals.bubble.loading) {
      return <Spinner />;
    } else if (!this.props.visuals.bubble.loaded) {
      this.props.loadStatesModel(1, undefined, undefined, undefined, 500);
      return <Spinner />;
    }

    const width = this.visualWrapper.current
      ? this.visualWrapper.current.offsetWidth
      : 500;

    return (
      <BubbleChart
        bubbleClickFun={console.log}
        graph={{
          zoom: 1.0,
          offsetX: 0.0,
          offsetY: 0.0
        }}
        showLegend={false}
        width={width - 300}
        height={width - 300}
        padding={0}
        valueFont={{
          family: 'Arial',
          size: 12,
          color: '#fff',
          weight: 'bold',
          align: 'center'
        }}
        labelFont={{
          family: 'Arial',
          size: 12,
          color: '#fff',
          weight: 'bold',
          align: 'center'
        }}
        data={this.props.visuals.bubble.data}
      />
    );
  }

  render(): React.ReactNode {
    return (
      <div>
        <Background useImage={true} />
        <Container className="Content__Container Visualizations">
          <div ref={this.visualWrapper}>
            <Row className="Cards">
              <Tabs
                id="tabs"
                activeKey={this.state.key}
                onSelect={(key: string): void => this.setState({ key })}
              >
                <Tab eventKey="state" title="State Map">
                  <h1>US map of emissions per state</h1>
                  {this.renderStateMap()}
                </Tab>
                <Tab eventKey="bubble" title="Bubble Chart">
                  <h1>Bubble chart of laws passed per state</h1>
                  {this.renderBubbleChart()}
                </Tab>
                <Tab eventKey="pie" title="Pie Chart">
                  <h1>Pie chart of total emissions by sector</h1>
                  {this.renderPieChart()}
                </Tab>
              </Tabs>
            </Row>
          </div>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState): { visuals: VisualState } => ({
  visuals: state.visuals
});

const mapDispatchToProps = {
  loadStatesModel,
  loadEmissionsModel
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Visualizations);
