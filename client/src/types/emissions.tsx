import { PageArgs, FilterResponse } from './';

export interface EmissionsModelState {
  loaded: boolean;
  loading: boolean;
  data: EmissionsModel[];
  filters: FilterResponse;
  args: PageArgs;
}

export interface EmissionsModel {
  emission_name: string;
  total_emissions: number;
  emission_pic_url: string;
  best_state: string;
  worst_year: number;
  worst_state: string;
  laws_passed: number;
}

export interface EmissionsInstanceState {
  loaded: boolean;
  loading: boolean;
  data?: EmissionsInstance;
}

export interface EmissionsInstance {
  emission_name: string;
  description: string;
  emission_pic_url: string;
  states: {
    [state: string]: {
      [year: number]: number;
    };
  };
  globals: {
    [year: number]: number;
  };
  tips: string[];
  legislation: [
    {
      id: number;
      name: string;
    }
  ];
  laws_passed: number;
  worst_state: string;
  best_state: string;
  worst_year: number;
}

export interface EmissionsModelResponse {
  args: PageArgs;
  data: EmissionsModel[];
  filters: FilterResponse;
}

export interface EmissionsInstanceResponse {
  args: {};
  data: EmissionsInstance;
}

export interface EmissionsState {
  instance: EmissionsInstanceState;
  model: EmissionsModelState;
}

export const LOAD_EMISSIONS_MODEL = 'LOAD_EMISSIONS_MODEL';
export const LOAD_EMISSIONS_MODEL_SUCCESS = 'LOAD_EMISSIONS_MODEL_SUCCESS';
export const LOAD_EMISSIONS_MODEL_FAILURE = 'LOAD_EMISSIONS_MODEL_FAILURE';

export const LOAD_EMISSIONS_INSTANCE = 'LOAD_EMISSIONS_INSTANCE';
export const LOAD_EMISSIONS_INSTANCE_SUCCESS =
  'LOAD_EMISSIONS_INSTANCE_SUCCESS';
export const LOAD_EMISSIONS_INSTANCE_FAILURE =
  'LOAD_EMISSIONS_INSTANCE_FAILURE';

interface LoadEmissionsModelAction {
  type: typeof LOAD_EMISSIONS_MODEL;
  types: [string, string, string];
  payload: {
    request: {
      url: string;
    };
  };
}

interface LoadEmissionsModelSuccessAction {
  type: typeof LOAD_EMISSIONS_MODEL_SUCCESS;
  payload: {
    data: EmissionsModelResponse;
  };
  meta: object;
}

interface LoadEmissionsModelFailureAction {
  type: typeof LOAD_EMISSIONS_MODEL_FAILURE;
  error: object;
  meta: object;
}

interface LoadEmissionsInstanceAction {
  type: typeof LOAD_EMISSIONS_INSTANCE;
  types: [string, string, string];
  payload: {
    request: {
      url: string;
    };
  };
}

interface LoadEmissionsInstanceSuccessAction {
  type: typeof LOAD_EMISSIONS_INSTANCE_SUCCESS;
  payload: {
    data: EmissionsInstanceResponse;
  };
  meta: object;
}

interface LoadEmissionsInstanceFailureAction {
  type: typeof LOAD_EMISSIONS_INSTANCE_FAILURE;
  error: object;
  meta: object;
}

export type EmissionsModelActionTypes =
  | LoadEmissionsModelAction
  | LoadEmissionsModelSuccessAction
  | LoadEmissionsModelFailureAction;
export type EmissionsInstanceActionTypes =
  | LoadEmissionsInstanceAction
  | LoadEmissionsInstanceSuccessAction
  | LoadEmissionsInstanceFailureAction;
export type EmissionsActionTypes =
  | EmissionsModelActionTypes
  | EmissionsInstanceActionTypes;
