import { PageArgs, FilterResponse } from './';

export interface LawsModelState {
  loaded: boolean;
  loading: boolean;
  data: LawsModel[];
  args: PageArgs;
  filters: FilterResponse;
}

export interface LawsModel {
  id: number;
  state_name: string;
  creation_date: string;
  updated_date: string;
  law_name: string;
  description: string;
  representatives: string[];
  url: string;
  emissions: string[];
}

export interface LawsInstanceState {
  loaded: boolean;
  loading: boolean;
  data?: LawsInstance;
}

export interface LawsInstance {
  id: string;
  state_name: string;
  creation_date: string;
  updated_date: string;
  law_name: string;
  description: string;
  representatives: string;
  url: string;
  emissions: string[];
}

export interface LawsModelResponse {
  args: PageArgs;
  data: LawsModel[];
  filters: FilterResponse;
}

export interface LawsInstanceResponse {
  args: {};
  data: LawsInstance;
}

export interface LawsState {
  instance: LawsInstanceState;
  model: LawsModelState;
}

export const LOAD_LAWS_MODEL = 'LOAD_LAWS_MODEL';
export const LOAD_LAWS_MODEL_SUCCESS = 'LOAD_LAWS_MODEL_SUCCESS';
export const LOAD_LAWS_MODEL_FAILURE = 'LOAD_LAWS_MODEL_FAILURE';

export const LOAD_LAWS_INSTANCE = 'LOAD_LAWS_INSTANCE';
export const LOAD_LAWS_INSTANCE_SUCCESS = 'LOAD_LAWS_INSTANCE_SUCCESS';
export const LOAD_LAWS_INSTANCE_FAILURE = 'LOAD_LAWS_INSTANCE_FAILURE';

interface LoadLawsModelAction {
  type: typeof LOAD_LAWS_MODEL;
  types: [string, string, string];
  payload: {
    request: {
      url: string;
    };
  };
}

interface LoadLawsModelSuccessAction {
  type: typeof LOAD_LAWS_MODEL_SUCCESS;
  payload: {
    data: LawsModelResponse;
  };
  meta: object;
}

interface LoadLawsModelFailureAction {
  type: typeof LOAD_LAWS_MODEL_FAILURE;
  error: object;
  meta: object;
}

interface LoadLawsInstanceAction {
  type: typeof LOAD_LAWS_INSTANCE;
  types: [string, string, string];
  payload: {
    request: {
      url: string;
    };
  };
}

interface LoadLawsInstanceSuccessAction {
  type: typeof LOAD_LAWS_INSTANCE_SUCCESS;
  payload: {
    data: LawsInstanceResponse;
  };
  meta: object;
}

interface LoadLawsInstanceFailureAction {
  type: typeof LOAD_LAWS_INSTANCE_FAILURE;
  error: object;
  meta: object;
}

export type LawsModelActionTypes =
  | LoadLawsModelAction
  | LoadLawsModelSuccessAction
  | LoadLawsModelFailureAction;
export type LawsInstanceActionTypes =
  | LoadLawsInstanceAction
  | LoadLawsInstanceSuccessAction
  | LoadLawsInstanceFailureAction;
export type LawsActionTypes = LawsModelActionTypes | LawsInstanceActionTypes;
