export interface GitLabCommit {
  author_email: string;
  author_name: string;
  committed_email: string;
  committed_name: string;
}

export interface GitLabIssue {
  author: {
    name: string;
    username: string;
  };
  closed_by: {
    name: string;
    username: string;
  };
}

export interface GitLabTest {
  username: string;
  name: string;
  tests: number;
}

export interface GitLabState {
  commits: GitLabCommit[];
  issues: GitLabIssue[];
  tests: GitLabTest[];
  loading: boolean;
  loaded: boolean;
}

export const LOAD_GITLAB = 'LOAD_GITLAB';
export const LOAD_GITLAB_SUCCESS = 'LOAD_GITLAB_SUCCESS';
export const LOAD_GITLAB_FAILURE = 'LOAD_GITLAB_FAILURE';

interface LoadGitLabAction {
  type: typeof LOAD_GITLAB;
  types: [string, string, string];
  payload: {
    request: {
      url: string;
    };
  };
}

interface LoadGitLabSuccessAction {
  type: typeof LOAD_GITLAB_SUCCESS;
  payload: {
    data: {
      commits: GitLabCommit[];
      issues: GitLabIssue[];
      tests: GitLabTest[];
    };
  };
  meta: Record<string, string>;
}

interface LoadGitLabFailureAction {
  type: typeof LOAD_GITLAB_FAILURE;
  error: Record<string, string>;
  meta: Record<string, string>;
}

export type GitLabActionTypes =
  | LoadGitLabAction
  | LoadGitLabSuccessAction
  | LoadGitLabFailureAction;
