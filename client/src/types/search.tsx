export interface EmissionSearchResult {
  emission_name: string;
  description: string;
}

export interface StateSearchResult {
  id: number;
  state_name: string;
}

export interface LawSearchResult {
  id: number;
  law_name: string;
  description: string;
}

export interface SearchArgs {
  search: string;
}

export interface LawSearchResponse {
  args: SearchArgs;
  data: LawSearchResult[];
}

export interface EmissionSearchResponse {
  args: SearchArgs;
  data: EmissionSearchResult[];
}

export interface StateSearchResponse {
  args: SearchArgs;
  data: StateSearchResult[];
}

export interface GlobalSearchResponse {
  args: SearchArgs;
  data: {
    emissions: EmissionSearchResult[];
    laws: LawSearchResult[];
    states: StateSearchResult[];
  };
}

export interface SearchState extends GlobalSearchResponse {
  loaded: boolean;
  loading: boolean;
}

export const GLOBAL_SEARCH = 'GLOBAL_SEARCH';
export const GLOBAL_SEARCH_SUCCESS = 'GLOBAL_SEARCH_SUCCESS';
export const GLOBAL_SEARCH_FAILURE = 'GLOBAL_SEARCH_FAILURE';

export const LAW_SEARCH = 'LAW_SEARCH';
export const LAW_SEARCH_SUCCESS = 'LAW_SEARCH_SUCCESS';
export const LAW_SEARCH_FAILURE = 'LAW_SEARCH_FAILURE';

export const STATE_SEARCH = 'STATE_SEARCH';
export const STATE_SEARCH_SUCCESS = 'STATE_SEARCH_SUCCESS';
export const STATE_SEARCH_FAILURE = 'STATE_SEARCH_FAILURE';

export const EMISSION_SEARCH = 'EMISSION_SEARCH';
export const EMISSION_SEARCH_SUCCESS = 'EMISSION_SEARCH_SUCCESS';
export const EMISSION_SEARCH_FAILURE = 'EMISSION_SEARCH_FAILURE';

interface GlobalSearchAction {
  type: typeof GLOBAL_SEARCH;
  types: [string, string, string];
  payload: {
    request: {
      url: string;
    };
  };
}

interface GlobalSearchSuccessAction {
  type: typeof GLOBAL_SEARCH_SUCCESS;
  payload: {
    data: GlobalSearchResponse;
  };
}

interface GlobalSearchFailureAction {
  type: typeof GLOBAL_SEARCH_FAILURE;
  error: object;
  meta: object;
}

interface LawSearchAction {
  type: typeof LAW_SEARCH;
  types: [string, string, string];
  payload: {
    request: {
      url: string;
    };
  };
}

interface LawSearchSuccessAction {
  type: typeof LAW_SEARCH_SUCCESS;
  payload: {
    data: LawSearchResponse;
  };
}

interface LawSearchFailureAction {
  type: typeof LAW_SEARCH_FAILURE;
  error: object;
  meta: object;
}

interface StateSearchAction {
  type: typeof STATE_SEARCH;
  types: [string, string, string];
  payload: {
    request: {
      url: string;
    };
  };
}

interface StateSearchSuccessAction {
  type: typeof STATE_SEARCH_SUCCESS;
  payload: {
    data: StateSearchResponse;
  };
}

interface StateSearchFailureAction {
  type: typeof STATE_SEARCH_FAILURE;
  error: object;
  meta: object;
}

interface EmissionSearchAction {
  type: typeof EMISSION_SEARCH;
  types: [string, string, string];
  payload: {
    request: {
      url: string;
    };
  };
}

interface EmissionSearchSuccessAction {
  type: typeof EMISSION_SEARCH_SUCCESS;
  payload: {
    data: EmissionSearchResponse;
  };
}

interface EmissionSearchFailureAction {
  type: typeof EMISSION_SEARCH_FAILURE;
  error: object;
  meta: object;
}

export type SearchActionTypes =
  | GlobalSearchAction
  | GlobalSearchSuccessAction
  | GlobalSearchFailureAction
  | LawSearchAction
  | LawSearchSuccessAction
  | LawSearchFailureAction
  | StateSearchAction
  | StateSearchSuccessAction
  | StateSearchFailureAction
  | EmissionSearchAction
  | EmissionSearchSuccessAction
  | EmissionSearchFailureAction;
