import { EmissionsActionTypes } from './emissions';
import { StatesActionTypes } from './states';
import { LawsActionTypes } from './laws';

export interface PageArgs {
  page: number;
  resultsPerPage: number;
  lastPage: number;
  sortDir: string;
  sortName: string;
  filters: AppliedFilters;
}

export type AppliedFilters = { [key: string]: string | number };

export type AllActions =
  | EmissionsActionTypes
  | StatesActionTypes
  | LawsActionTypes;

export * from './gitlab';
export * from './emissions';
export * from './states';
export * from './laws';
export * from './filters';
export * from './search';
export * from './visuals';
