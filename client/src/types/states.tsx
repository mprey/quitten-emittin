import { PageArgs, FilterResponse } from './';

export interface StatesModelState {
  loaded: boolean;
  loading: boolean;
  data: StatesModel[];
  args: PageArgs;
  filters: FilterResponse;
}

export interface StatesModel {
  state_name: string;
  population: number;
  flag_url: string;
  num_representatives: number;
  emissions: number;
  largest_contributor: string;
  laws_passed: number;
}

export interface StatesInstanceState {
  loaded: boolean;
  loading: boolean;
  data?: StatesInstance;
}

export interface StatesInstance {
  state_name: string;
  population: number;
  flag_url: string;
  num_representatives: number;
  emissions: {
    [name: string]: {
      [year: number]: number;
    };
  };
  legislation: [
    {
      id: number;
      name: string;
    }
  ];
}

export interface StatesModelResponse {
  args: PageArgs;
  data: StatesModel[];
  filters: FilterResponse;
}

export interface StatesInstanceResponse {
  args: {};
  data: StatesInstance;
}

export interface StatesState {
  model: StatesModelState;
  instance: StatesInstanceState;
}

export const LOAD_STATES_MODEL = 'LOAD_STATES_MODEL';
export const LOAD_STATES_MODEL_SUCCESS = 'LOAD_STATES_MODEL_SUCCESS';
export const LOAD_STATES_MODEL_FAILURE = 'LOAD_STATES_MODEL_FAILURE';

export const LOAD_STATES_INSTANCE = 'LOAD_STATES_INSTANCE';
export const LOAD_STATES_INSTANCE_SUCCESS = 'LOAD_STATES_INSTANCE_SUCCESS';
export const LOAD_STATES_INSTANCE_FAILURE = 'LOAD_STATES_INSTANCE_FAILURE';

interface LoadStatesModelAction {
  type: typeof LOAD_STATES_MODEL;
  types: [string, string, string];
  payload: {
    request: {
      url: string;
    };
  };
}

interface LoadStatesModelSuccessAction {
  type: typeof LOAD_STATES_MODEL_SUCCESS;
  payload: {
    data: StatesModelResponse;
  };
  meta: object;
}

interface LoadStatesModelFailureAction {
  type: typeof LOAD_STATES_MODEL_FAILURE;
  error: object;
  meta: object;
}

interface LoadStatesInstanceAction {
  type: typeof LOAD_STATES_INSTANCE;
  types: [string, string, string];
  payload: {
    request: {
      url: string;
    };
  };
}

interface LoadStatesInstanceSuccessAction {
  type: typeof LOAD_STATES_INSTANCE_SUCCESS;
  payload: {
    data: StatesInstanceResponse;
  };
  meta: object;
}

interface LoadStatesInstanceFailureAction {
  type: typeof LOAD_STATES_INSTANCE_FAILURE;
  error: object;
  meta: object;
}

export type StatesModelActionTypes =
  | LoadStatesModelAction
  | LoadStatesModelSuccessAction
  | LoadStatesModelFailureAction;
export type StatesInstanceActionTypes =
  | LoadStatesInstanceAction
  | LoadStatesInstanceSuccessAction
  | LoadStatesInstanceFailureAction;
export type StatesActionTypes =
  | StatesModelActionTypes
  | StatesInstanceActionTypes;
