export interface Filter {
  display: string;
  key: string;
  type: FilterType;
  values?: string[];
}

export type FilterResponse = { [key: string]: string[] };

export type FilterType = 'number' | 'string';
