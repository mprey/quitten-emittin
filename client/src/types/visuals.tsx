export interface VisualState {
  bubble: {
    loaded: boolean;
    loading: boolean;
    data: {
      label: string;
      value: number;
    }[];
  };
  stateMap: {
    loaded: boolean;
    loading: boolean;
    data: { id: string; total: number }[];
  };
  pieChart: {
    loaded: boolean;
    loading: boolean;
    data: { label: string; value: number }[];
  };
}
