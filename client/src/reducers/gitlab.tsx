import {
  GitLabState,
  GitLabActionTypes,
  LOAD_GITLAB,
  LOAD_GITLAB_SUCCESS,
  LOAD_GITLAB_FAILURE
} from '../types';

const initialState: GitLabState = {
  commits: [],
  issues: [],
  tests: [],
  loaded: false,
  loading: false
};

export default function reducer(
  state = initialState,
  action: GitLabActionTypes
): GitLabState {
  switch (action.type) {
    case LOAD_GITLAB: {
      return {
        ...state,
        loading: true
      };
    }
    case LOAD_GITLAB_SUCCESS: {
      console.log(action);
      return {
        ...state,
        loading: false,
        loaded: true,
        ...action.payload.data
      };
    }
    case LOAD_GITLAB_FAILURE: {
      return {
        ...state,
        loading: false,
        loaded: false
      };
    }
    default:
      return state;
  }
}
