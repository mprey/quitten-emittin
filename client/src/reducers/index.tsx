import { createStore, applyMiddleware, combineReducers } from 'redux';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import { default as gitLabReducer } from './gitlab';
import { default as statesReducer } from './states';
import { default as emissionsReducer } from './emissions';
import { default as lawsReducer } from './laws';
import { default as searchReducer } from './search';
import { default as visualsReducer } from './visuals';

declare let process: {
  env: {
    NODE_ENV: string;
  };
};

const client = axios.create({
  baseURL:
    process.env.NODE_ENV === 'development'
      ? 'http://localhost:5000'
      : 'https://api.quittinemittin.me',
  responseType: 'json'
});

const rootReducer = combineReducers({
  gitlab: gitLabReducer,
  states: statesReducer,
  emissions: emissionsReducer,
  laws: lawsReducer,
  search: searchReducer,
  visuals: visualsReducer
});

export type AppState = ReturnType<typeof rootReducer>;

export const store = createStore(
  rootReducer,
  applyMiddleware(axiosMiddleware(client))
);
