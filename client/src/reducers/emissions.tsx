import {
  EmissionsState,
  EmissionsActionTypes,
  LOAD_EMISSIONS_INSTANCE,
  LOAD_EMISSIONS_INSTANCE_SUCCESS,
  LOAD_EMISSIONS_INSTANCE_FAILURE,
  LOAD_EMISSIONS_MODEL,
  LOAD_EMISSIONS_MODEL_FAILURE,
  LOAD_EMISSIONS_MODEL_SUCCESS
} from '../types';

const initialState: EmissionsState = {
  instance: {
    loaded: false,
    loading: false,
    data: undefined
  },
  model: {
    loaded: false,
    loading: false,
    data: [],
    args: {
      page: 0,
      lastPage: 0,
      resultsPerPage: 0,
      sortDir: '',
      sortName: '',
      filters: {}
    },
    filters: {}
  }
};

export default function reducer(
  state = initialState,
  action: EmissionsActionTypes
): EmissionsState {
  switch (action.type) {
    case LOAD_EMISSIONS_INSTANCE: {
      return {
        ...state,
        instance: {
          ...state.instance,
          loading: true
        }
      };
    }

    case LOAD_EMISSIONS_MODEL: {
      return {
        ...state,
        model: {
          ...state.model,
          loading: true
        }
      };
    }

    case LOAD_EMISSIONS_INSTANCE_SUCCESS: {
      return {
        ...state,
        instance: {
          loading: false,
          loaded: true,
          data: action.payload.data.data
        }
      };
    }

    case LOAD_EMISSIONS_MODEL_SUCCESS: {
      return {
        ...state,
        model: {
          loading: false,
          loaded: true,
          data: action.payload.data.data,
          args: action.payload.data.args,
          filters: action.payload.data.filters
        }
      };
    }

    case LOAD_EMISSIONS_MODEL_FAILURE: {
      return {
        ...state,
        model: {
          ...state.model,
          loading: false,
          loaded: false
        }
      };
    }

    case LOAD_EMISSIONS_INSTANCE_FAILURE: {
      return {
        ...state,
        instance: {
          ...state.instance,
          loading: false,
          loaded: false
        }
      };
    }
    default:
      return state;
  }
}
