import {
  SearchState,
  SearchActionTypes,
  GLOBAL_SEARCH,
  GLOBAL_SEARCH_SUCCESS,
  GLOBAL_SEARCH_FAILURE,
  LAW_SEARCH_FAILURE,
  STATE_SEARCH_FAILURE,
  EMISSION_SEARCH_FAILURE,
  LAW_SEARCH,
  STATE_SEARCH,
  EMISSION_SEARCH,
  LAW_SEARCH_SUCCESS,
  EMISSION_SEARCH_SUCCESS,
  STATE_SEARCH_SUCCESS
} from '../types';

const initialState: SearchState = {
  loaded: false,
  loading: false,
  args: {
    search: ''
  },
  data: {
    emissions: [],
    states: [],
    laws: []
  }
};

export default function reducer(
  state = initialState,
  action: SearchActionTypes
): SearchState {
  switch (action.type) {
    case LAW_SEARCH:
    case STATE_SEARCH:
    case EMISSION_SEARCH:
    case GLOBAL_SEARCH: {
      return {
        ...state,
        loading: true
      };
    }
    case GLOBAL_SEARCH_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        ...action.payload.data
      };
    }
    case LAW_SEARCH_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        data: {
          laws: action.payload.data.data,
          emissions: [],
          states: []
        }
      };
    }
    case STATE_SEARCH_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        data: {
          states: action.payload.data.data,
          emissions: [],
          laws: []
        }
      };
    }
    case EMISSION_SEARCH_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        data: {
          emissions: action.payload.data.data,
          states: [],
          laws: []
        }
      };
    }
    case LAW_SEARCH_FAILURE:
    case STATE_SEARCH_FAILURE:
    case EMISSION_SEARCH_FAILURE:
    case GLOBAL_SEARCH_FAILURE: {
      return {
        ...state,
        loading: false,
        loaded: false
      };
    }
    default:
      return state;
  }
}
