import {
  StatesState,
  StatesActionTypes,
  LOAD_STATES_INSTANCE,
  LOAD_STATES_INSTANCE_SUCCESS,
  LOAD_STATES_INSTANCE_FAILURE,
  LOAD_STATES_MODEL,
  LOAD_STATES_MODEL_FAILURE,
  LOAD_STATES_MODEL_SUCCESS
} from '../types';

const initialState: StatesState = {
  instance: {
    loaded: false,
    loading: false,
    data: undefined
  },
  model: {
    loaded: false,
    loading: false,
    data: [],
    args: {
      page: 0,
      lastPage: 0,
      resultsPerPage: 0,
      sortDir: '',
      sortName: '',
      filters: {}
    },
    filters: {}
  }
};

export default function reducer(
  state = initialState,
  action: StatesActionTypes
): StatesState {
  switch (action.type) {
    case LOAD_STATES_INSTANCE: {
      return {
        ...state,
        instance: {
          ...state.instance,
          loading: true
        }
      };
    }

    case LOAD_STATES_MODEL: {
      return {
        ...state,
        model: {
          ...state.model,
          loading: true
        }
      };
    }

    case LOAD_STATES_INSTANCE_SUCCESS: {
      return {
        ...state,
        instance: {
          loading: false,
          loaded: true,
          data: action.payload.data.data
        }
      };
    }

    case LOAD_STATES_MODEL_SUCCESS: {
      return {
        ...state,
        model: {
          loading: false,
          loaded: true,
          data: action.payload.data.data,
          args: action.payload.data.args,
          filters: action.payload.data.filters
        }
      };
    }

    case LOAD_STATES_MODEL_FAILURE: {
      return {
        ...state,
        model: {
          ...state.model,
          loading: false,
          loaded: false
        }
      };
    }

    case LOAD_STATES_INSTANCE_FAILURE: {
      return {
        ...state,
        instance: {
          ...state.instance,
          loading: false,
          loaded: false
        }
      };
    }
    default:
      return state;
  }
}
