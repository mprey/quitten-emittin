import {
  VisualState,
  AllActions,
  LOAD_STATES_MODEL,
  LOAD_STATES_MODEL_SUCCESS,
  LOAD_EMISSIONS_MODEL,
  LOAD_EMISSIONS_MODEL_SUCCESS
} from '../types';

const initialState: VisualState = {
  bubble: {
    loaded: false,
    loading: false,
    data: []
  },
  stateMap: {
    loaded: false,
    loading: false,
    data: []
  },
  pieChart: {
    loaded: false,
    loading: false,
    data: []
  }
};

export default function reducer(
  state = initialState,
  action: AllActions
): VisualState {
  switch (action.type) {
    case LOAD_EMISSIONS_MODEL: {
      return {
        ...state,
        pieChart: {
          loaded: false,
          loading: true,
          data: []
        }
      };
    }

    case LOAD_EMISSIONS_MODEL_SUCCESS: {
      return {
        ...state,
        pieChart: {
          loaded: true,
          loading: false,
          data: action.payload.data.data.map(item => ({
            label: item.emission_name,
            value: item.total_emissions
          }))
        }
      };
    }
    case LOAD_STATES_MODEL: {
      return {
        ...state,
        bubble: {
          loaded: false,
          loading: true,
          data: []
        },
        stateMap: {
          loaded: false,
          loading: true,
          data: []
        }
      };
    }
    case LOAD_STATES_MODEL_SUCCESS: {
      return {
        ...state,
        bubble: {
          loaded: action.payload.data.data.length >= 50,
          loading: false,
          data: action.payload.data.data.map(item => ({
            label: item.state_name,
            value: item.laws_passed
          }))
        },
        stateMap: {
          loaded: action.payload.data.data.length >= 50,
          loading: false,
          data: action.payload.data.data.map(item => ({
            id: item.state_name,
            total: item.emissions
          }))
        }
      };
    }
    default:
      return state;
  }
}
