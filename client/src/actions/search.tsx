import {
  GLOBAL_SEARCH,
  GLOBAL_SEARCH_SUCCESS,
  GLOBAL_SEARCH_FAILURE,
  SearchActionTypes,
  LAW_SEARCH,
  LAW_SEARCH_SUCCESS,
  LAW_SEARCH_FAILURE,
  STATE_SEARCH,
  STATE_SEARCH_SUCCESS,
  STATE_SEARCH_FAILURE,
  EMISSION_SEARCH,
  EMISSION_SEARCH_SUCCESS,
  EMISSION_SEARCH_FAILURE
} from '../types';

export function globalSearch(search: string): SearchActionTypes {
  return {
    type: GLOBAL_SEARCH,
    types: [GLOBAL_SEARCH, GLOBAL_SEARCH_SUCCESS, GLOBAL_SEARCH_FAILURE],
    payload: {
      request: {
        url: '/api/v1/search/global?search=' + search
      }
    }
  };
}

export function lawSearch(search: string): SearchActionTypes {
  return {
    type: LAW_SEARCH,
    types: [LAW_SEARCH, LAW_SEARCH_SUCCESS, LAW_SEARCH_FAILURE],
    payload: {
      request: {
        url: '/api/v1/search/laws?search=' + search
      }
    }
  };
}

export function stateSearch(search: string): SearchActionTypes {
  return {
    type: STATE_SEARCH,
    types: [STATE_SEARCH, STATE_SEARCH_SUCCESS, STATE_SEARCH_FAILURE],
    payload: {
      request: {
        url: '/api/v1/search/states?search=' + search
      }
    }
  };
}

export function emissionSearch(search: string): SearchActionTypes {
  return {
    type: EMISSION_SEARCH,
    types: [EMISSION_SEARCH, EMISSION_SEARCH_SUCCESS, EMISSION_SEARCH_FAILURE],
    payload: {
      request: {
        url: '/api/v1/search/emissions?search=' + search
      }
    }
  };
}
