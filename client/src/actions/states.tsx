import {
  StatesActionTypes,
  LOAD_STATES_MODEL,
  LOAD_STATES_MODEL_SUCCESS,
  LOAD_STATES_MODEL_FAILURE,
  LOAD_STATES_INSTANCE,
  LOAD_STATES_INSTANCE_SUCCESS,
  LOAD_STATES_INSTANCE_FAILURE,
  AppliedFilters
} from '../types';

export function loadStatesModel(
  page: number,
  sortName?: string,
  sortDir?: string,
  filters?: AppliedFilters,
  resultsPerPage?: number
): StatesActionTypes {
  return {
    type: LOAD_STATES_MODEL,
    types: [
      LOAD_STATES_MODEL,
      LOAD_STATES_MODEL_SUCCESS,
      LOAD_STATES_MODEL_FAILURE
    ],
    payload: {
      request: {
        url:
          '/api/v1/states?page=' +
          page +
          (sortName ? `&sortName=${sortName}` : '') +
          (sortDir ? `&sortDir=${sortDir}` : '') +
          (filters ? `&filters=${JSON.stringify(filters)}` : '') +
          (resultsPerPage ? `&resultsPerPage=${resultsPerPage}` : '')
      }
    }
  };
}

export function loadStatesInstance(stateName: string): StatesActionTypes {
  return {
    type: LOAD_STATES_INSTANCE,
    types: [
      LOAD_STATES_INSTANCE,
      LOAD_STATES_INSTANCE_SUCCESS,
      LOAD_STATES_INSTANCE_FAILURE
    ],
    payload: {
      request: {
        url: '/api/v1/states/' + stateName
      }
    }
  };
}
