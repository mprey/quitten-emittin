import {
  LawsActionTypes,
  LOAD_LAWS_MODEL,
  LOAD_LAWS_MODEL_SUCCESS,
  LOAD_LAWS_MODEL_FAILURE,
  LOAD_LAWS_INSTANCE,
  LOAD_LAWS_INSTANCE_SUCCESS,
  LOAD_LAWS_INSTANCE_FAILURE,
  AppliedFilters
} from '../types';

export function loadLawsModel(
  page: number,
  sortName?: string,
  sortDir?: string,
  filters?: AppliedFilters
): LawsActionTypes {
  return {
    type: LOAD_LAWS_MODEL,
    types: [LOAD_LAWS_MODEL, LOAD_LAWS_MODEL_SUCCESS, LOAD_LAWS_MODEL_FAILURE],
    payload: {
      request: {
        url:
          '/api/v1/laws?page=' +
          page +
          (sortName ? `&sortName=${sortName}` : '') +
          (sortDir ? `&sortDir=${sortDir}` : '') +
          (filters ? `&filters=${JSON.stringify(filters)}` : '')
      }
    }
  };
}

export function loadLawsInstance(emissionName: string): LawsActionTypes {
  return {
    type: LOAD_LAWS_INSTANCE,
    types: [
      LOAD_LAWS_INSTANCE,
      LOAD_LAWS_INSTANCE_SUCCESS,
      LOAD_LAWS_INSTANCE_FAILURE
    ],
    payload: {
      request: {
        url: '/api/v1/laws/' + emissionName
      }
    }
  };
}
