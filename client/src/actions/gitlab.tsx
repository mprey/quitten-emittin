import {
  GitLabActionTypes,
  LOAD_GITLAB,
  LOAD_GITLAB_SUCCESS,
  LOAD_GITLAB_FAILURE
} from '../types';

export function loadGitLabStatistics(): GitLabActionTypes {
  return {
    type: LOAD_GITLAB,
    types: [LOAD_GITLAB, LOAD_GITLAB_SUCCESS, LOAD_GITLAB_FAILURE],
    payload: {
      request: {
        url: '/api/v1/gitlab'
      }
    }
  };
}
