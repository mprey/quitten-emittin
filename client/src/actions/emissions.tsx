import {
  EmissionsActionTypes,
  LOAD_EMISSIONS_MODEL,
  LOAD_EMISSIONS_MODEL_SUCCESS,
  LOAD_EMISSIONS_MODEL_FAILURE,
  LOAD_EMISSIONS_INSTANCE,
  LOAD_EMISSIONS_INSTANCE_SUCCESS,
  LOAD_EMISSIONS_INSTANCE_FAILURE,
  AppliedFilters
} from '../types';

export function loadEmissionsModel(
  page: number,
  sortName?: string,
  sortDir?: string,
  filters?: AppliedFilters
): EmissionsActionTypes {
  return {
    type: LOAD_EMISSIONS_MODEL,
    types: [
      LOAD_EMISSIONS_MODEL,
      LOAD_EMISSIONS_MODEL_SUCCESS,
      LOAD_EMISSIONS_MODEL_FAILURE
    ],
    payload: {
      request: {
        url:
          '/api/v1/emissions?page=' +
          page +
          (sortName ? `&sortName=${sortName}` : '') +
          (sortDir ? `&sortDir=${sortDir}` : '') +
          (filters ? `&filters=${JSON.stringify(filters)}` : '')
      }
    }
  };
}

export function loadEmissionsInstance(
  emissionName: string
): EmissionsActionTypes {
  return {
    type: LOAD_EMISSIONS_INSTANCE,
    types: [
      LOAD_EMISSIONS_INSTANCE,
      LOAD_EMISSIONS_INSTANCE_SUCCESS,
      LOAD_EMISSIONS_INSTANCE_FAILURE
    ],
    payload: {
      request: {
        url: '/api/v1/emissions/' + emissionName
      }
    }
  };
}
