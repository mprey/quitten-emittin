import React from 'react';
import {routerTestProps} from '../../misc/generateProps';
import {Emission} from '../../containers/';
import { mount } from '../../setupTests';
import { MemoryRouter as Router } from 'react-router-dom';
import { EmissionsInstanceState } from '../../types';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

const mockStore = configureStore();
const mockDispatchfn = jest.fn();

describe('<Emission />', () => {
  let wrapper: any, store: any;
  const props: any = {
    ...routerTestProps('/emissions/:name', { name: 'Coal' })
  };

  beforeEach(() => {
    const initialState: EmissionsInstanceState = {
      loaded: false,
      loading: true,
      data: undefined
    };

    store = mockStore({emissions: { instance: initialState }});
    wrapper = mount(
      <Provider store={store}>
        <Router>
          <Emission {...props} dispatch={mockDispatchfn} />
        </Router>
      </Provider>
    );
  });

  it('defines the component', () => {
    expect(wrapper).toBeDefined();
  });

  it('renders form component', () => {
    expect(wrapper.find('.Emission').first()).toHaveLength(1);
  });
});