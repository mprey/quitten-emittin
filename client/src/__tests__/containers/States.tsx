import React from 'react';
import {routerTestProps} from '../../misc/generateProps';
import {States} from '../../containers/';
import { mount } from '../../setupTests';
import { MemoryRouter as Router } from 'react-router-dom';
import { StatesModelState } from '../../types';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

const mockStore = configureStore();
const mockDispatchfn = jest.fn();

describe('<States />', () => {
  let wrapper: any, store: any;
  const props: any = {
    ...routerTestProps('/states/', {})
  };

  beforeEach(() => {
    const initialState: StatesModelState = {
      loaded: false,
      loading: false,
      args: {
        resultsPerPage: 0,
        lastPage: 0,
        page: 0,
      },
      data: []
    };

    store = mockStore({states: { model: initialState }});
    wrapper = mount(
      <Provider store={store}>
        <Router>
          <States {...props} dispatch={mockDispatchfn} />
        </Router>
      </Provider>
    );
  });

  it('defines the component', () => {
    expect(wrapper).toBeDefined();
  });

  it('renders form component', () => {
    expect(wrapper.find('.States').first()).toHaveLength(1);
  });
});