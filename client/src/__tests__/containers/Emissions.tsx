import React from 'react';
import {routerTestProps} from '../../misc/generateProps';
import {Emissions} from '../../containers/';
import { mount } from '../../setupTests';
import { MemoryRouter as Router } from 'react-router-dom';
import { EmissionsModelState } from '../../types';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

const mockStore = configureStore();
const mockDispatchfn = jest.fn();

describe('<Emissions />', () => {
  let wrapper: any, store: any;
  const props: any = {
    ...routerTestProps('/emissions/', {})
  };

  beforeEach(() => {
    const initialState: EmissionsModelState = {
      loaded: false,
      loading: false,
      args: {
        resultsPerPage: 0,
        lastPage: 0,
        page: 0,
      },
      data: []
    };

    store = mockStore({emissions: { model: initialState }});
    wrapper = mount(
      <Provider store={store}>
        <Router>
          <Emissions {...props} dispatch={mockDispatchfn} />
        </Router>
      </Provider>
    );
  });

  it('defines the component', () => {
    expect(wrapper).toBeDefined();
  });

  it('renders form component', () => {
    expect(wrapper.find('.Emissions').first()).toHaveLength(1);
  });
});