import React from 'react';
import {About} from '../../containers/';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { mount } from '../../setupTests';
import { GitLabState } from '../../types';

const mockStore = configureStore();
const mockDispatchfn = jest.fn();

describe('<About />', () => {
  let wrapper: any, store: any;
  const props: any = {};

  beforeEach(() => {
    const initialState: GitLabState = {
      loaded: false,
      commits: [],
      issues: [],
      tests: [],
      loading: false
    };

    store = mockStore({gitlab: initialState});

    wrapper = mount(
      <Provider store={store}>
        <About {...props} dispatch={mockDispatchfn} />
      </Provider>,
    );
  });

  it('defines the component', () => {
    expect(wrapper).toBeDefined();
  });

  it('renders form component', () => {
    expect(wrapper.find('.About').first()).toHaveLength(1);
  });
});