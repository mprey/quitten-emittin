import React from 'react';
import {routerTestProps} from '../../misc/generateProps';
import {Law} from '../../containers/';
import { mount } from '../../setupTests';
import { MemoryRouter as Router } from 'react-router-dom';
import { LawsInstanceState } from '../../types';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

const mockStore = configureStore();
const mockDispatchfn = jest.fn();

describe('<Law />', () => {
  let wrapper: any, store: any;
  const props: any = {
    ...routerTestProps('/law/:id', { id: "1" })
  };

  beforeEach(() => {
    const initialState: LawsInstanceState = {
      loaded: false,
      loading: true,
      data: undefined
    };

    store = mockStore({laws: { instance: initialState }});
    wrapper = mount(
      <Provider store={store}>
        <Router>
          <Law {...props} dispatch={mockDispatchfn} />
        </Router>
      </Provider>
    );
  });

  it('defines the component', () => {
    expect(wrapper).toBeDefined();
  });

  it('renders form component', () => {
    expect(wrapper.find('.Law').first()).toHaveLength(1);
  });
});