import React from 'react';
import {routerTestProps} from '../../misc/generateProps';
import {State} from '../../containers/';
import { mount } from '../../setupTests';
import { MemoryRouter as Router } from 'react-router-dom';
import { StatesInstanceState } from '../../types';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

const mockStore = configureStore();
const mockDispatchfn = jest.fn();

describe('<State />', () => {
  let wrapper: any, store: any;
  const props: any = {
    ...routerTestProps('/state/:name', { name: "Texas" })
  };

  beforeEach(() => {
    const initialState: StatesInstanceState = {
      loaded: false,
      loading: true,
      data: undefined
    };

    store = mockStore({states: { instance: initialState }});
    wrapper = mount(
      <Provider store={store}>
        <Router>
          <State {...props} dispatch={mockDispatchfn} />
        </Router>
      </Provider>
    );
  });

  it('defines the component', () => {
    expect(wrapper).toBeDefined();
  });

  it('renders form component', () => {
    expect(wrapper.find('.State').first()).toHaveLength(1);
  });
});