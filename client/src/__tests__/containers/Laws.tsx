import React from 'react';
import {routerTestProps} from '../../misc/generateProps';
import {Laws} from '../../containers/';
import { mount } from '../../setupTests';
import { MemoryRouter as Router } from 'react-router-dom';
import { LawsModelState } from '../../types';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

const mockStore = configureStore();
const mockDispatchfn = jest.fn();

describe('<Laws />', () => {
  let wrapper: any, store: any;
  const props: any = {
    ...routerTestProps('/laws/', {})
  };

  beforeEach(() => {
    const initialState: LawsModelState = {
      loaded: false,
      loading: false,
      args: {
        resultsPerPage: 0,
        lastPage: 0,
        page: 0,
      },
      data: []
    };

    store = mockStore({laws: { model: initialState }});
    wrapper = mount(
      <Provider store={store}>
        <Router>
          <Laws {...props} dispatch={mockDispatchfn} />
        </Router>
      </Provider>
    );
  });

  it('defines the component', () => {
    expect(wrapper).toBeDefined();
  });

  it('renders form component', () => {
    expect(wrapper.find('.Laws__Container').first()).toHaveLength(1);
  });
});