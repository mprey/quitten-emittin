import React from 'react';
import {Navbar} from '../../components/';
import { mount } from '../../setupTests';
import { MemoryRouter as Router } from 'react-router-dom';

describe('<Navbar />', () => {
  let wrapper: any, store: any;
  const props: any = {
    userImage: false
  };

  beforeEach(() => {
    wrapper = mount(
      <Router>
        <Navbar {...props}/>       
      </Router>
    );
  });

  it('defines the component', () => {
    expect(wrapper).toBeDefined();
  });

  it('renders form component', () => {
    expect(wrapper.find('.navbar').first()).toHaveLength(1);
  });
});