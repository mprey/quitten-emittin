import React from 'react';
import {Footer} from '../../components/';
import { mount } from '../../setupTests';
import { MemoryRouter as Router } from 'react-router-dom';

describe('<Footer />', () => {
  let wrapper: any, store: any;
  const props: any = {
    userImage: false
  };

  beforeEach(() => {
    wrapper = mount(
      <Router>
        <Footer {...props}/>       
      </Router>
    );
  });

  it('defines the component', () => {
    expect(wrapper).toBeDefined();
  });

  it('renders form component', () => {
    expect(wrapper.find('.Footer').first()).toHaveLength(1);
  });
});