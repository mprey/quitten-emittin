import React from 'react';
import {ScrollToTop} from '../../components/';
import { mount } from '../../setupTests';
import { MemoryRouter as Router } from 'react-router-dom';

describe('<ScrollToTop />', () => {
  let wrapper: any, store: any;
  const props: any = {
    userImage: false
  };

  beforeEach(() => {
    wrapper = mount(
      <Router>
        <ScrollToTop {...props}>
            {null}
        </ScrollToTop>       
      </Router>
    );
  });

  it('defines the component', () => {
    expect(wrapper).toBeDefined();
  });
});