import React from 'react';
import {Background} from '../../components/';
import { mount } from '../../setupTests';

describe('<Background />', () => {
  let wrapper: any, store: any;
  const props: any = {
    userImage: false
  };

  beforeEach(() => {
    wrapper = mount(
      <Background {...props}/>
    );
  });

  it('defines the component', () => {
    expect(wrapper).toBeDefined();
  });

  it('renders form component', () => {
    expect(wrapper.find('.Background').first()).toHaveLength(1);
  });
});