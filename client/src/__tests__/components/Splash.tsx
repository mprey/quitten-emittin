import React from 'react';
import {Splash} from '../../components/';
import { mount } from '../../setupTests';
import { MemoryRouter as Router } from 'react-router-dom';

describe('<Splash />', () => {
  let wrapper: any, store: any;
  const props: any = {
    userImage: false
  };

  beforeEach(() => {
    wrapper = mount(
      <Router>
        <Splash {...props}/>       
      </Router>
    );
  });

  it('defines the component', () => {
    expect(wrapper).toBeDefined();
  });

  it('renders form component', () => {
    expect(wrapper.find('.Splash').first()).toHaveLength(1);
  });
});