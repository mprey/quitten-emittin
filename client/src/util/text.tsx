export function fixText(text: string, textLimit: number): string {
  if (text.length > textLimit) {
    return text.slice(0, text.indexOf(' ', textLimit)) + '...';
  }
  return text;
}
